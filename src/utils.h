#ifndef UTILS_H
#define UTILS_H

#include <string>
#include <sstream>
#include <opencv2/opencv.hpp>
#include <fstream>

using namespace std; 
using namespace cv;

/**
* Helper method to convert an integer to a string
*/
string IntToString (int a);

/**
* Helper method to read the specialised homography-files, which contain the ground truth about the homography between two files.
* The homography returned describes how to transform a point, taken from the first image to the second image
*/
cv::Mat readHomography (string pathToHomography);

vector<KeyPoint> epipolarLinePrediction(vector<KeyPoint> input, Mat R, Mat t);

/**
 * Helper-method that calculates the euclidean distance between two points
 */
double euclideanDist(Point p, Point q);

/**
 * Comparer to sort keypoints ascending by their response value
 */
bool keypointComparer (KeyPoint k1,KeyPoint k2);

#endif