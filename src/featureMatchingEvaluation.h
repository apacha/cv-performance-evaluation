#ifndef FEATUREMATCHINGEVALUATION_H
#define FEATUREMATCHINGEVALUATION_H

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc_c.h>
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/nonfree/nonfree.hpp"
#include "opencv2/imgproc/imgproc_c.h"
#include "opencv2/legacy/compat.hpp"
#include <opencv2/opencv.hpp>
#include <stdio.h>
#include <time.h>
#include <string>
#include "utils.h"

using namespace cv;
using namespace std;

/** 
* Evaluates run-times for different feature matchers (based on previously detected features and descriptors).
*	featureDetector The feature detector that should be used. Any of  {"FAST", "STAR", "SIFT", "SURF", "ORB", "BRISK", "MSER", "GFTT", "HARRIS", "Dense", "SimpleBlob"}
*	featureDescriptor The feature descriptor that should be used. Any of { "SIFT", "SURF", "ORB", "BRISK", "BRIEF", "FREAK" };
*	featureDescriptorNeedsConversion Flag indicating, whether the feature descriptor needs conversion. True, if a binary descriptor is used (e.g. ORB, BRISK, BRIEF, FREAK), false otherwise.
*	useNN Flag indicating whether Nearest Neighborr (true) or k-Nearest Neighbour (false) with k=3 should be performed.
*/
void runFeatureMatchingEvaluation(string featureDetector, string featureDescriptor, bool featureDescriptorsNeedConversion, bool useNN);

/**
* This evaluates feature matching quality by comparing the matching results to brute-force search with homography
*	featureDetector The feature detector that should be used. Any of  {"FAST", "STAR", "SIFT", "SURF", "ORB", "BRISK", "MSER", "GFTT", "HARRIS", "Dense", "SimpleBlob"}
*	featureDescriptor The feature descriptor that should be used. Any of { "SIFT", "SURF", "ORB", "BRISK", "BRIEF", "FREAK" };
*	featureDescriptorNeedsConversion Flag indicating, whether the feature descriptor needs conversion. True, if a binary descriptor is used (e.g. ORB, BRISK, BRIEF, FREAK), false otherwise.
*/
void runFeatureMatchingQualityEvaluation(string featureDetector, string featureDescriptor, bool featureDescriptorsNeedConversion);

#endif