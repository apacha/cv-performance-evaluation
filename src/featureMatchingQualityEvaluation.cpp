#include "featureMatchingEvaluation.h"

typedef struct {
	// Relevant matches
	vector<DMatch> matches;
	// Relevant, because distance <= epsilon
	int nrOfKeypointsMatched;
	// Irrelevant, because distance > epsilon
	int nrOfKeypointsUnmatched;
} bfMatchingResults;

typedef struct {
	// TBD
	double precision;
	// TBD
	double recall;
} statistics;

/**
* This method performs a brute-force search to determine matches between translated keypoints, limited by a maximum epsilon error that controls the matching.
* This method returns a "ground-truth" since it transforms the points from the second image with the known homography and then performs nearest neighbor matching.
*/
bfMatchingResults bruteForceMatching (vector<KeyPoint> keypoints1, vector<KeyPoint> keypoints2, Mat homography, double maxEpsilon) {

	bfMatchingResults results;
	results.nrOfKeypointsMatched = results.nrOfKeypointsUnmatched = 0;

	for(vector<KeyPoint>::size_type i = 0; i != keypoints2.size(); i++) {
		// Read the detected Keypoint
		KeyPoint keyPointInSecondImage = keypoints2[i];

		// Construct a 3x1 homogenised vector
		Matx31f locationInSecondImage (keyPointInSecondImage.pt.x, keyPointInSecondImage.pt.y, 1);
		// Estimate where the keypoint should be in the original image (by applying the known homography)
		Mat locationInFirstImage = homography * Mat(locationInSecondImage);
		// Get the point and normalise it to back from homogenised coordinate system.
		Point2f projectedPointInFirstImage = Point2f(locationInFirstImage.at<float>(0,0)/locationInFirstImage.at<float>(2,0),locationInFirstImage.at<float>(1,0)/locationInFirstImage.at<float>(2,0));

		bool found = false;
		double minEpsilon = 100000;
		int keypoints1Index = -1;
		// Check if the point also exists in the first keypoint-set
		for(vector<KeyPoint>::size_type k = 0; k != keypoints1.size(); k++) {
			double dist = euclideanDist(keypoints1[k].pt, projectedPointInFirstImage);
			if(dist <= maxEpsilon && dist < minEpsilon) {
				minEpsilon = dist;
				keypoints1Index = k;
				found = true;
			}
		}
		if(found == true) {
			results.matches.push_back(DMatch(i, keypoints1Index, minEpsilon));
			results.nrOfKeypointsMatched++;
		} else {
			results.nrOfKeypointsUnmatched++;
		}
	}

	return results;
}

/**
* This method filters all matches to only allow if their distance (after projecting them) is smaller than a maximum epsilon.
*
* Remark: You cannot use match.distance, since it is not the euclidean distance but potentially another metric like hamming distance!
*/
vector<DMatch> filterMatches(std::vector<DMatch> matches, vector<KeyPoint> image1Keypoints, vector<KeyPoint> image2Keypoints, Mat homography, double maxEpsilon) {
	vector<DMatch> filteredMatches;

	for(vector<DMatch>::size_type m = 0; m != matches.size(); m++) {
		// Read the detected Keypoint
		KeyPoint keyPointInSecondImage = image2Keypoints[matches[m].queryIdx];
		// Construct a 3x1 homogenised vector
		Matx31f locationInSecondImage (keyPointInSecondImage.pt.x, keyPointInSecondImage.pt.y, 1);
		// Estimate where the keypoint should be in the original image (by applying the known homography)
		Mat locationInFirstImage = homography * Mat(locationInSecondImage);
		Point2f projectedPointInFirstImage = Point2f(locationInFirstImage.at<float>(0,0)/locationInFirstImage.at<float>(2,0),locationInFirstImage.at<float>(1,0)/locationInFirstImage.at<float>(2,0));

		bool found = false;
		// Check if the point also exists in the first keypoint-set
		if(euclideanDist(image1Keypoints[matches[m].trainIdx].pt, projectedPointInFirstImage) <= maxEpsilon) {
			filteredMatches.push_back(matches[m]);
		}
	}

	return filteredMatches;
}

/**
* This method calculates the statistics for this image-pair (precision, recall)
*/
statistics evaluateMatches(bfMatchingResults groundTruth, vector<DMatch> matches) {
	// The total number of keypoints detected
	int total = groundTruth.nrOfKeypointsMatched + groundTruth.nrOfKeypointsUnmatched;
	// The total number of relevant keypoints, found by bf with ground-truth
	int totalRelevantPoints = groundTruth.nrOfKeypointsMatched;
	// The total number of irrelevant keypoints, found by bf with ground-truth
	int totalIrrelevantPoints = groundTruth.nrOfKeypointsUnmatched;
	// The total number of found points
	int totalFoundPoints = matches.size();
	// The total number of not found points
	int totalNotFoundPoints = total - totalFoundPoints;
	// The number of points that were found in groundTruth and by matching algorithm
	int numberOfRelevantFound = 0;
	// Calculate the intersection between those two sets
	for(vector<DMatch>::size_type i = 0; i != matches.size(); i++) {
		// Read the detected match
		DMatch match1 = matches[i];

		for(vector<DMatch>::size_type j = 0; j != groundTruth.matches.size(); j++) {
			// Read the detected match
			DMatch match2 =  groundTruth.matches[j];

			if(match1.queryIdx == match2.queryIdx && match1.trainIdx == match2.trainIdx) {
				numberOfRelevantFound ++;
			}
		}
	}

	// The number of irrelevant points, which were found by the matching algorithm
	int numberOfIrrelevantFound = totalFoundPoints - numberOfRelevantFound;
	// The number of points that were not found, but were relevant
	int numberOfNotFoundRelevant = totalRelevantPoints - numberOfRelevantFound;

	// Sanity-check
	if(totalNotFoundPoints - numberOfNotFoundRelevant != totalIrrelevantPoints - numberOfIrrelevantFound) {
		cout << "Sanity-check failed! There is something wrong here!" << endl;
	}

	statistics result;
	if(totalFoundPoints != 0)
		result.precision = ((double)numberOfRelevantFound / (totalFoundPoints));
	else
		result.precision = 0;
	result.recall = ((double)numberOfRelevantFound / (totalRelevantPoints));

	return result;
}

/**
* This evaluation feature matching quality by comparing the matching results to brute-force search with homography
*	featureDetector The feature detector that should be used. Any of  {"FAST", "STAR", "SIFT", "SURF", "ORB", "BRISK", "MSER", "GFTT", "HARRIS", "Dense", "SimpleBlob"}
*	featureDescriptor The feature descriptor that should be used. Any of { "SIFT", "SURF", "ORB", "BRISK", "BRIEF", "FREAK" };
*	featureDescriptorNeedsConversion Flag indicating, whether the feature descriptor needs conversion. True, if a binary descriptor is used (e.g. ORB, BRISK, BRIEF, FREAK), false otherwise.
*/
void runFeatureMatchingQualityEvaluation(string featureDetector, string featureDescriptor, bool featureDescriptorsNeedConversion)
{
	// Path to the testdata-directory
#ifdef __APPLE__
	string testdataDirectory = "/Users/Head/Dropbox/Performance Evaluation Paper/Testdata/";
#else
	string testdataDirectory = "../../testdata/";
#endif

	// Initialise the test-data

	// Specify the size of the testFolders-array
	const int numberOfScenes = 10;
	const int picturesPerScene = 6;
	// These are the folders that must exist in testdataDirectory in order to run the evaluation
	string testFolders[numberOfScenes]  = {"Bark", "Bikes", "Boat", "Christchurch", "Graf", "Leuven","Memorial", "Trees", "Ubc", "Wall"};
	// Path to all individual images (8 folders/scenes with each 6 images, so 48 images in total)
	vector<vector<string> > testFiles;
	// Path to all homography-files (8 folders/scene with 5 transformed images in each folder)
	vector<vector<string> > homographyFiles;

	// Construct the file-paths
	for(int i=0; i<numberOfScenes; i++)  {
		// Construct the paths to the images
		testFiles.push_back(vector<string>());
		for(int j = 1; j<=6; j++) {
			testFiles[i].push_back(testdataDirectory + testFolders[i] + "/img" + IntToString(j) + ".png");
		}
		// Construct the paths to the homographies
		homographyFiles.push_back(vector<string>());
		for(int j = 2; j<=6; j++) {
			homographyFiles[i].push_back(testdataDirectory + testFolders[i] + "/H1to" + IntToString(j) + "p");
		}
	}

	// Total number of matchers is 12: 6 different matchers from generic Matcher, 6 configuration for FLANN matcher
	const int numberOfGenericMatchers = 6;
	const int numberOfFlannMatchers = 6;

	// Array of different matching strategies for Generic Matcher
	string genericMatcher[numberOfGenericMatchers] = {"BruteForce", "BruteForce-L1", "BruteForce-Hamming", "BruteForce-Hamming(2)", "BruteForce-SL2", "FlannBased"};
	bool genericNeedConversion[numberOfGenericMatchers] = {false, false, false, false, false, true};
	// Flags, indicating, whether these matchers allow non-binary descriptors
	bool genericAllowsNonBinary[numberOfGenericMatchers] = {true, true, false, false, true, true};

	// The IndexParams-object to initialise the generic FLANN-matcher object
	flann::IndexParams *flannMatcherParams[numberOfFlannMatchers] = {new flann::LinearIndexParams(), new flann::KDTreeIndexParams(), new flann::KMeansIndexParams(), new flann::CompositeIndexParams(), new flann::LshIndexParams(20,10,2), new flann::AutotunedIndexParams()};
	// The names to be displayed
	string flannMatcherNames[numberOfFlannMatchers]  = {"Linear Brute Force", "KD-Tree (4 trees)", "K-Means", "Composite (KD-Tree and K-Means)", "Locality Sensitive Hashing", "Autotuned based on data"};
	// Boolean, indicating if features have to be converted from binary to float. True, if the matching does not allow binary descriptors
	bool flannNeedConversion[numberOfFlannMatchers] = {true, true, true, true, false, true};
	// Flags, indicating, whether these matchers allow non-binary descriptors
	bool flannAllowsNonBinary[numberOfFlannMatchers] = {true, true, true, true, false, true};

	// Maximum number of keypoints that are allowed. Very high (> 30000) to allow unbounded number. ORB default is 500
	const int maxNumberOfKeypoints = 500;

	// Initialise the Feature detector and feature descriptor
	Ptr<FeatureDetector> detector = FeatureDetector::create(featureDetector);
	Ptr<DescriptorExtractor> extractor = DescriptorExtractor::create(featureDescriptor);

	// Start the total clock that will measure the overall-time for everything
	clock_t tTotalStart = clock();
	// Total number of features detected
	int numberOfFeaturesDetected = 0;

	// Maximal distance to control the overlap-error
	const int numberOfEpsilonRuns = 10;
	double maxEpsilons[numberOfEpsilonRuns] = {2,3,4,5,6,7,8,9,10,11};

	// Array to store all the runtime from all matchers. The first 6 elements are for generic matchers, the other 6 are for flann-based matchers
	statistics statisticResults[numberOfGenericMatchers + numberOfFlannMatchers][numberOfEpsilonRuns];
	for(int i=0; i<numberOfGenericMatchers + numberOfFlannMatchers; i++) {
		for(int j=0; j<numberOfEpsilonRuns; j++) {
			statisticResults[i][j].precision = 0;
			statisticResults[i][j].recall = 0;
		}
	}

	// The array for references to all flann-matchers.
	FlannBasedMatcher flannMatcher[numberOfFlannMatchers];
	// Initialise Flann-Matchers
	for(int l=0; l<numberOfFlannMatchers; l++) {
		flannMatcher[l] = FlannBasedMatcher(flannMatcherParams[l]);
	}

	// Evaluate performance
	for(int i=0; i<numberOfScenes; i++)  {
		// Load reference image
		Mat image1 = imread(testFiles[i][0], CV_LOAD_IMAGE_GRAYSCALE);
		// Output the currently evaluated scene
		cout << endl << "Scene: " << testFolders[i] << endl;

		// Calculate keypoints
		vector<KeyPoint> image1Keypoints;
		detector->detect(image1, image1Keypoints);
		sort(image1Keypoints.begin(),image1Keypoints.end(), keypointComparer);
		if(image1Keypoints.size() > maxNumberOfKeypoints) {
			// Take the first 500 elements, which are the best keypoints according to their response
			image1Keypoints = vector<KeyPoint>(image1Keypoints.begin(),image1Keypoints.begin()+maxNumberOfKeypoints); 
		} 
			
		std::cout << "Number of "<< featureDetector << " Keypoints: " << image1Keypoints.size() << std::endl;
		numberOfFeaturesDetected += image1Keypoints.size();
		
		// Mats for the feature descriptors
		Mat image1Descriptors, image1DescriptorsFloat;
		// Calculate features from keypoints
		extractor->compute(image1, image1Keypoints, image1Descriptors);

		// If the data is binary (e.g. ORB Descriptor) and the algorithm expects floats and does not allow binary, convert the descriptors 
		if(image1Descriptors.type()!=CV_32F && featureDescriptorsNeedConversion) {
			image1Descriptors.convertTo(image1DescriptorsFloat, CV_32F);
		}

		for(int j=1; j<6; j++) {
			// Load transformed images
			Mat image2 = imread(testFiles[i][j], CV_LOAD_IMAGE_GRAYSCALE);
			// Output the currently evaluated scene
			cout << endl << "Comparing to: " << testFiles[i][j] << endl;
			// Get the correct homography
			Mat homography = readHomography(homographyFiles[i][j-1]);
			// Invert the homography to go from the second image to the first image (instead of from the first to the second)
			invert(homography,homography);
			// Output the homography for debugging purposes
			//cout << "M = " << endl << " " << homography << endl << endl;

			// Calculate keypoints
			vector<KeyPoint> image2Keypoints;
			detector->detect(image2, image2Keypoints);	
			sort(image2Keypoints.begin(),image2Keypoints.end(), keypointComparer);
			if(image2Keypoints.size() > maxNumberOfKeypoints) {
				// Take the first 500 elements, which are the best keypoints according to their response
				image2Keypoints = vector<KeyPoint>(image2Keypoints.begin(),image2Keypoints.begin()+maxNumberOfKeypoints); 
			} 

			std::cout << "Number of "<< featureDetector << " Keypoints: " << image2Keypoints.size() << std::endl;
			numberOfFeaturesDetected += image2Keypoints.size();

			// Mats for the feature descriptors
			Mat image2Descriptors, image2DescriptorsFloat;
			// Calculate features from keypoints
			extractor->compute(image2, image2Keypoints, image2Descriptors);

			// If the data is binary (e.g. ORB Descriptor) and the algorithm expects floats and does not allow binary, convert the descriptors 
			if(image2Descriptors.type()!=CV_32F && featureDescriptorsNeedConversion) {
				image2Descriptors.convertTo(image2DescriptorsFloat, CV_32F);
			}

			for(int k=0; k<numberOfEpsilonRuns; k++) {
				// Perform brute-force matching in order to get the ground-truth
				bfMatchingResults groundTruth= bruteForceMatching(image1Keypoints, image2Keypoints, homography, maxEpsilons[k]);
				
				//// -----------------------------------
				// Evaluate the matching
				//// -----------------------------------

				// Iterate over all different generic matchers
				for(int l=0; l<numberOfGenericMatchers; l++) {
					// featureDescriptorsNeedConversion is true, when the features are binary, so everything is fine, if it is true.
					// genericAllowsNonBinary is true, when non-binary are supported, so everything is fine, if it is true.
					if(!featureDescriptorsNeedConversion && !genericAllowsNonBinary[l]) {
						// Filter the cases, where Hamming-distance is required, but descriptor is non-binary:
						// featureDescriptorsNeedConversion = false (its non-binary) && genericAllowsNonBinary = false (doesn't allow non-binary)
						continue;
					}

					// Run with generic Matcher
					Ptr<DescriptorMatcher> matcher = DescriptorMatcher::create(genericMatcher[l]);
					// Vector for the results
					std::vector<DMatch> nnMatches;
					// Perform matching - Use converted or binary features, depending on what the matcher is expecting
					try{
						// Perform NN-Matching
						if(genericNeedConversion[l] && featureDescriptorsNeedConversion) {
							matcher->match(image2DescriptorsFloat, image1DescriptorsFloat, nnMatches);
						} else {
							matcher->match(image2Descriptors, image1Descriptors, nnMatches);
						}

					} catch (Exception ex) {
						cout << ex.msg;
					}

					// Filter matches to only allow a maximum epsilon (same as with the brute-force matching)	
					std::vector<DMatch> realMatches = filterMatches(nnMatches, image1Keypoints, image2Keypoints, homography, maxEpsilons[k]);

					// Evaluate matches against each other
					statistics result = evaluateMatches(groundTruth, realMatches);

					statisticResults[l][k].precision += result.precision;
					statisticResults[l][k].recall += result.recall;

					cout << genericMatcher[l] << "\tPrecision: " << result.precision << "\tRecall: " << result.recall << endl;
				}


				// Iterate over all different flann matchers
				for(int l=0; l<numberOfFlannMatchers; l++) {
					// featureDescriptorsNeedConversion is true, when the features are binary, so everything is fine, if it is true.
					// flannAllowsNonBinary is true, when non-binary are supported, so everything is fine, if it is true.
					if(!featureDescriptorsNeedConversion && !flannAllowsNonBinary[l]) {
						// Filter the cases, where Hamming-distance is required, but descriptor is non-binary:
						// featureDescriptorsNeedConversion = false (its non-binary) && flannAllowsNonBinary = false (doesn't allow non-binary)
						continue;
					}

					// Run FLANN-based matcher (initialisation has happened before)
					// Vector for the results
					std::vector<DMatch> nnMatches;
					// Perform matching - Use converted or binary features, depending on what the matcher is expecting
					try{
						// Perform NN-Matching
						if(flannNeedConversion[l] && featureDescriptorsNeedConversion) {
							flannMatcher[l].match(image2DescriptorsFloat, image1DescriptorsFloat, nnMatches);
						} else {
							flannMatcher[l].match(image2Descriptors, image1Descriptors, nnMatches);
						}

					} catch (Exception ex) {
						cout << ex.msg;
					}

					// Filter matches to only allow a maximum epsilon	
					std::vector<DMatch> realMatches = filterMatches(nnMatches, image1Keypoints, image2Keypoints, homography, maxEpsilons[k]);

					// Evaluate matches against each other
					statistics result = evaluateMatches(groundTruth, realMatches);

					statisticResults[l + numberOfGenericMatchers][k].precision += result.precision;
					statisticResults[l + numberOfGenericMatchers][k].recall += result.recall;

					cout << flannMatcherNames[l] << "\tPrecision: " << result.precision << "\tRecall: " << result.recall << endl;
				}
			}
		}
	}

	cout << endl << "Statistical results" << endl << "Used epsilons: ";

	for(int k=0; k<numberOfEpsilonRuns; k++) {
		cout << maxEpsilons[k] << ";";
	}
	cout << endl;

	// Output summary of all results for the generic matchers
	for(int i=0; i<numberOfGenericMatchers; i++) {
		cout << genericMatcher[i] << " for " << featureDetector << "/" << featureDescriptor << " configuration" << endl;

		cout << "\tPrecision: ";
		for(int k=0; k<numberOfEpsilonRuns; k++) {
			cout << statisticResults[i][k].precision/(numberOfScenes * picturesPerScene) << ";";
		}

		cout << endl << "\tRecall: ";
		for(int k=0; k<numberOfEpsilonRuns; k++) {
			cout << statisticResults[i][k].recall/(numberOfScenes * picturesPerScene) << ";";
		}

		cout << endl;
	}

	// Output summary of all results for the flann matchers
	for(int i=0; i<numberOfFlannMatchers; i++) {
		cout << flannMatcherNames[i] << " for " << featureDetector << "/" << featureDescriptor << " configuration" << endl;

		cout << "\tPrecision: ";
		for(int k=0; k<numberOfEpsilonRuns; k++) {
			cout << statisticResults[i + numberOfGenericMatchers][k].precision/(numberOfScenes * picturesPerScene) << ";";
		}

		cout << endl << "\tRecall: ";
		for(int k=0; k<numberOfEpsilonRuns; k++) {
			cout << statisticResults[i + numberOfGenericMatchers][k].recall/(numberOfScenes * picturesPerScene) << ";";
		}

		cout << endl;
	}

	// Output statistics
	cout << "Total average number of keypoints: " << numberOfFeaturesDetected / (numberOfScenes * picturesPerScene) << endl;
	cout << "Total run-time for quality matching evaluation: " << ((double)(clock() - tTotalStart)/CLOCKS_PER_SEC) << "s" << endl;

	return;
}

