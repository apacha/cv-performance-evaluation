#include "utils.h"

/**
* Helper method to convert an integer to a string
*/
string IntToString (int a)
{
	string s;
	stringstream out;
	out << a;
	s = out.str();
	return s;
}


/**
* Helper method to read the specialised homography-files, which contain the ground truth about the homography between two files.
* The homography returned describes how to transform a point, taken from the first image to the second image
*/
cv::Mat readHomography (string pathToHomography)
{
	cv::Mat homography(3,3, CV_32F);

	string line;
	int lineNumber=0,columnNumber=0; 
	ifstream myfile (pathToHomography.c_str());
	if (myfile.is_open())
	{
		while ( myfile.good() )
		{
			getline (myfile,line);
			// Each line contains three values, separated by two spaces like this: "-1.5735788289619667E-5  1.0242951905091244E-5  1.0" or "-0.3499531830464912  -0.1975486500576974  466.54576370699766"
			istringstream iss(line);
			columnNumber=0;
			do
			{
				string sub;
				iss >> sub;
				if(sub != "") {
					//cout << "Substring: " << sub << endl;
					double val = atof(sub.c_str());
					homography.row(lineNumber).col(columnNumber) = val;
				}
				columnNumber++;
			} while (iss);

			lineNumber++;
			//cout << line << endl;
		}
		myfile.close();
	}

	return homography;
}

// This function is incomplete but will eventually be used to predict the epipolar lines for each Keypoint in the new image
vector<KeyPoint> epipolarLinePrediction(vector<KeyPoint> input, Mat R, Mat t) {
    
    vector<KeyPoint> predicted;
    Mat y_prev(3, 1, CV_32F);
    Mat y_p_1(3,1,CV_32F);
    Mat y_p_2(3,1,CV_32F);
    
    Mat x_p_1(3,1,CV_32F);
    Mat x_p_2(3,1,CV_32F);
    
    float f_arb;
    float z_arb;
    
    //for (KeyPoint &pointer: input) {
    //    
    //    // Assigning data to image world coordinates
    //    y_prev.row(1).col(1) = pointer.pt.x;
    //    y_prev.row(2).col(1) = pointer.pt.y;
    //    
    //    // Genrerating first random values for point on line
    //    f_arb = rand();
    //    z_arb = rand();
    //    
    //    // Assigning depth to previous point
    //    y_prev.row(3).col(1) = z_arb;
    //    
    //    // Predicting future point
    //    x_p_1 = z_arb/f_arb * R * y_p_1 + t;
    //    y_p_1 = f_arb/z_arb * x_p_1;
    //    
    //    // Regenerating random values on epipolar line f and x_3
    //    f_arb = rand();
    //    z_arb = rand();
    //    
    //    // Assigning depth to previous point
    //    y_prev.row(3).col(1) = z_arb;
    //    
    //    // Predicting future point
    //    x_p_1 = z_arb/f_arb * R * y_p_1 + t;
    //    y_p_1 = f_arb/z_arb * x_p_1;
    //}
	return input;
}

/**
 * Helper-method that calculates the euclidean distance between two points
 */
double euclideanDist(Point p, Point q)
{
    Point diff = p - q;
    return sqrt((double)diff.x*diff.x + diff.y*diff.y);
}

/**
 * Comparer to sort keypoints ascending by their response value
 */
bool keypointComparer (KeyPoint k1,KeyPoint k2) { return (k1.response>k2.response); }