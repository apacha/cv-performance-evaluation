#include "descriptors.h"

void calculateSurf (Mat image, bool calculateDescriptors) {
	try {
		Mat emptyMask, surfImageDescriptors;
		//Define structure for storing keypoints and descriptors
		std::vector<cv::KeyPoint> surfImageKeypoints = std::vector<cv::KeyPoint>();
		//cv::OutputArray imageDescriptors;
		// Initialise SURF with standard parameters
		SURF surf = SURF();
		// Run Surf
		if(calculateDescriptors) {	
			surf(image, emptyMask, surfImageKeypoints, surfImageDescriptors);
		} else {
			surf(image, emptyMask, surfImageKeypoints, cv::noArray());
		}

		if(surfImageKeypoints.size() != 0) {
			std::cout << "Number of SURF Keypoints/Descriptors: " << surfImageKeypoints.size() << std::endl;
		}

		/*for(std::vector<int>::size_type i = 0; i != surfImageKeypoints.size() && i < 100; i++) {
			std::cout <<  "SURF Keypoints " << i << ": " << surfImageKeypoints[i].response << std::endl;
		}*/

	} catch (cv::Exception exception) {
		std::cout << exception.msg<< std::endl;
	}
}

void calculateSurfOrb (Mat image) {
    try{
        Mat orbImageDescriptors;
        vector<KeyPoint> surfImageKeypoints;
        
        Ptr<FeatureDetector> detector;
		// For more algorithms see http://docs.opencv.org/modules/features2d/doc/common_interfaces_of_feature_detectors.html?highlight=featuredetector#featuredetector-create
        detector = FeatureDetector::create("SURF");
        detector->detect(image, surfImageKeypoints);
        
        Ptr<DescriptorExtractor> extractor;
        extractor = DescriptorExtractor::create("ORB");
        extractor->compute(image, surfImageKeypoints, orbImageDescriptors);
        if(surfImageKeypoints.size() != 0) {
            std::cout << "Number of SURF-Keypoints/ORB-Descriptors: " << surfImageKeypoints.size() << std::endl;
        }
    } catch (cv::Exception exception) {
        std::cout << exception.msg<< std::endl;
	}
}


void calculateSift (Mat image, bool calculateDescriptors) {
	try {
		Mat emptyMask, siftImageDescriptors;
		//Define structure for storing keypoints and descriptors
		std::vector<cv::KeyPoint> siftImageKeypoints = std::vector<cv::KeyPoint>();
		// Initialise SIFT with standard parameters
		SIFT sift = SIFT();
		// Run Sift
		if(calculateDescriptors) {
			sift(image, emptyMask, siftImageKeypoints, siftImageDescriptors);
		} else {
			sift(image, emptyMask, siftImageKeypoints, cv::noArray());
		}

		if(siftImageKeypoints.size() != 0) {
			std::cout << "Number of SIFT Keypoints/Descriptors: " << siftImageKeypoints.size() << std::endl;
		}

		/*for(std::vector<int>::size_type i = 0; i != siftImageKeypoints.size(); i++) {
			std::cout <<  "SIFT Keypoints " << i << ": " << siftImageKeypoints[i].response << std::endl;
		}*/

	} catch (cv::Exception exception) {
		std::cout << exception.msg<< std::endl;
	}
}

void calculateBrisk (Mat image, bool calculateDescriptors) {
	try {
		Mat emptyMask, briskImageDescriptors;
		//Define structure for storing keypoints and descriptors
		std::vector<cv::KeyPoint> briskImageKeypoints = std::vector<cv::KeyPoint>();
		// Initialise BRISK with standard parameters
		BRISK brisk = BRISK();
		// Run brisk
		if(calculateDescriptors) {
			brisk(image, emptyMask, briskImageKeypoints, briskImageDescriptors);
		} else {
			// This does not work due to a bug in openCV (http://code.opencv.org/issues/3130)
			brisk(image, emptyMask, briskImageKeypoints, cv::noArray());
		}

		if(briskImageKeypoints.size() != 0) {
			std::cout << "Number of BRISK Keypoints/Descriptors: " << briskImageKeypoints.size() << std::endl;
		}
	} catch (cv::Exception exception) {
		std::cout << exception.msg<< std::endl;
	}
}

void calculateOrb (Mat image, bool calculateDescriptors) {
	try {
		Mat emptyMask, orbImageDescriptors;
		//Define structure for storing keypoints and descriptors
		std::vector<cv::KeyPoint> orbImageKeypoints = std::vector<cv::KeyPoint>();
		// Initialise ORB with standard parameters
		ORB orb = ORB(6000, 1.2f, 8, 31, 0, 2, 0, 31);
		// Run orb
		if(calculateDescriptors) {
			orb(image, emptyMask, orbImageKeypoints, orbImageDescriptors);
		} else {
			orb(image, emptyMask, orbImageKeypoints,  cv::noArray());
		}

		if(orbImageKeypoints.size() != 0) {
			std::cout << "Number of ORB Keypoints/Descriptors: " << orbImageKeypoints.size() << std::endl;
		}
	} catch (cv::Exception exception) {
		std::cout << exception.msg<< std::endl;
	}
}

void calculateMser (Mat image) {
	try {
		Mat emptyMask;
		//Define structure for storing keypoints
		std::vector<std::vector<cv::Point> > mserImageKeypoints;
		// Initialise MSER with standard parameters
		MSER mser = MSER();
		// Run mser
		mser(image, mserImageKeypoints, emptyMask);

		if(mserImageKeypoints.size() != 0) {
			std::cout << "Number of MSER Keypoints: " << mserImageKeypoints.size() << std::endl;
		}
	} catch (cv::Exception exception) {
		std::cout << exception.msg<< std::endl;
	}
}

std::vector<cv::KeyPoint> calculateFast (Mat image) {
	try {
		Mat emptyMask;
		//Define structure for storing keypoints and descriptors
		std::vector<cv::KeyPoint> fastImageKeypoints = std::vector<cv::KeyPoint>();
		// Initialise FAST with threshold of 9 (TODO Check this value)
		FAST(image, fastImageKeypoints, 9);

		if(fastImageKeypoints.size() != 0) {
			std::cout << "Number of FAST Keypoints: " << fastImageKeypoints.size() << std::endl;
		}

		return fastImageKeypoints;
	} catch (cv::Exception exception) {
		std::cout << exception.msg<< std::endl;
	}
}

std::vector<Point2f> calculateShiTomasi (Mat image) {
	try {
		//Define structure for storing keypoints and parameters used by ShiTomasi
		vector<Point2f> corners;
		double qualityLevel = 0.01;
		double minDistance = 10;
		int blockSize = 3;
		bool useHarrisDetector = false;
		double k = 0.04;
		int maxCorners = 8000;

		/// Apply corner detection
		goodFeaturesToTrack( image,
			corners,
			maxCorners,
			qualityLevel,
			minDistance,
			Mat(),
			blockSize,
			useHarrisDetector,
			k );

		if(corners.size() != 0) {
			std::cout << "Number of ShiTomasi Keypoints: " << corners.size() << std::endl;
		}

		return corners;
	} catch (cv::Exception exception) {
		std::cout << exception.msg<< std::endl;
	}
}

void calculateFreak (Mat image, std::vector<cv::KeyPoint> freakImageKeypoints) {
	try {
		Mat emptyMask, freakImageDescriptors;
		// Initialise FREAK with standard parameters

		FREAK freak = FREAK();
		// Run Freak
		freak.compute(image, freakImageKeypoints, freakImageDescriptors);

		if(freakImageKeypoints.size() != 0) {
			std::cout << "Number of FREAK Descriptors: " << freakImageKeypoints.size() << std::endl;
		}
	} catch (cv::Exception exception) {
		std::cout << exception.msg<< std::endl;
	}
}
