#ifndef ORB_HOMOGRAPHY_H
#define ORB_HOMOGRAPHY_H

#include <stdio.h>
#include <iostream>
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/calib3d/calib3d.hpp"

using namespace std;
using namespace cv;

/**
* @function surf_homography
* @brief Calculates the surf-keypoints for two given images, matches them with a flann-matcher, calculates homography using RANSAC and displayes the two images
*/
int orb_homography( string filePath1, string filePath2 );

#endif