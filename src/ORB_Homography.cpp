#include "ORB_Homography.h"

int orb_homography( string filePath1, string filePath2 )
{
	Mat img_object1 = imread( filePath1, IMREAD_GRAYSCALE );
	Mat img_object2 = imread( filePath2, IMREAD_GRAYSCALE );

	if( !img_object1.data || !img_object2.data )
	{
		std::cout<< " --(!) Error reading images " << std::endl;
		return -1;
	}

	//-- Step 1: Detect the keypoints using ORB Detector
	int numOfPoints = 500;

	OrbFeatureDetector detector( numOfPoints );

	std::vector<KeyPoint> keypoints_object, keypoints_scene;

	detector.detect( img_object1, keypoints_object );
	detector.detect( img_object2, keypoints_scene );

	//-- Step 2: Calculate descriptors (feature binary strings)
	OrbDescriptorExtractor extractor;
	
	Mat descriptors_object, descriptors_scene;

	extractor.compute( img_object1, keypoints_object, descriptors_object );
	extractor.compute( img_object2, keypoints_scene, descriptors_scene );

	//-- Step 3: Matching descriptor vectors using FLANN matcher
	FlannBasedMatcher matcher (new flann::LshIndexParams(20,10,2));

	// Run much faster Clustering index, which requires the binary descriptors to be converted first
	/*FlannBasedMatcher matcher (new flann::HierarchicalClusteringIndexParams());
	if(descriptors_object.type()!=CV_32F) {
		descriptors_object.convertTo(descriptors_object, CV_32F);
		descriptors_scene.convertTo(descriptors_scene, CV_32F);
	}*/

	std::vector< DMatch > matches;
	try {
		matcher.match( descriptors_object, descriptors_scene, matches );
	} catch (Exception ex) {
		cout << ex.msg;
		return -1;
	}

	// TODO: Try to build index and perform knn-search 
	// (http://docs.opencv.org/modules/flann/doc/flann_fast_approximate_nearest_neighbor_search.html#flann-index-t-knnsearch)	
	std::vector< std::vector<DMatch> > knnMatches;
	matcher.knnMatch( descriptors_object, descriptors_scene, knnMatches, 5 );

	double max_dist = 0; double min_dist = 100;

	//-- Quick calculation of max and min distances between keypoints
	for( int i = 0; i < descriptors_object.rows; i++ )
	{ double dist = matches[i].distance;
	if( dist < min_dist ) min_dist = dist;
	if( dist > max_dist ) max_dist = dist;
	}

	printf("-- Max dist : %f \n", max_dist );
	printf("-- Min dist : %f \n", min_dist );

	//-- Draw only "good" matches (i.e. whose distance is less than 3*min_dist )
	std::vector< DMatch > good_matches;

	for( int i = 0; i < descriptors_object.rows; i++ )
	{ 
		//if( matches[i].distance < 3*min_dist )
		{ 
			good_matches.push_back( matches[i]); 
		}
	}

	Mat img_matches;
	drawMatches( img_object1, keypoints_object, img_object2, keypoints_scene,
		good_matches, img_matches, Scalar::all(-1), Scalar::all(-1),
		vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS );


	//-- Localize the object from img_1 in img_2
	std::vector<Point2f> obj;
	std::vector<Point2f> scene;

	for( size_t i = 0; i < good_matches.size(); i++ )
	{
		//-- Get the keypoints from the good matches
		obj.push_back( keypoints_object[ good_matches[i].queryIdx ].pt );
		scene.push_back( keypoints_scene[ good_matches[i].trainIdx ].pt );
	}

	if(scene.size() > 7) { // Check that findHomography is allowed to be called
		Mat H = findHomography( obj, scene, RANSAC );

		//-- Get the corners from the image_1 ( the object to be "detected" )
		std::vector<Point2f> obj_corners(4);
		obj_corners[0] = Point(0,0); 
		obj_corners[1] = Point( img_object1.cols, 0 );
		obj_corners[2] = Point( img_object1.cols, img_object1.rows ); 
		obj_corners[3] = Point( 0, img_object1.rows );
		std::vector<Point2f> scene_corners(4);

		perspectiveTransform( obj_corners, scene_corners, H);

		//-- Draw lines between the corners (the mapped object in the scene - image_2 )
		Point2f offset( (float)img_object1.cols, 0);
		line( img_matches, scene_corners[0] + offset, scene_corners[1] + offset, Scalar(0, 255, 0), 4 );
		line( img_matches, scene_corners[1] + offset, scene_corners[2] + offset, Scalar( 0, 255, 0), 4 );
		line( img_matches, scene_corners[2] + offset, scene_corners[3] + offset, Scalar( 0, 255, 0), 4 );
		line( img_matches, scene_corners[3] + offset, scene_corners[0] + offset, Scalar( 0, 255, 0), 4 );

		//-- Show detected matches
		cvNamedWindow(filePath2.c_str(), CV_WINDOW_AUTOSIZE); // creating a new window each time        
		imshow( filePath2.c_str(), img_matches );
		//waitKey(0);

		// Save detected image
		imwrite(filePath2 + ".comp.png", img_matches);
		return 0;
	} else {

		return -1;
	}



	
}
