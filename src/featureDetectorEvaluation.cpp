#include "featureDetectorEvaluation.h"

/**
 * This method evaluates the keypoints in the following way:
 * for each keypoint from keypoints2 {
 *   1. Calculate an epsilon-distance (nearest-neighbour euclidean distance) and calculate the target region area that should be searched
 *   2. Project keypoint from the second image via the homography back to their position in the first image
 *   3. Match projected position with every keypoint from keypoints1 and if their euclidean distance is smaller or equal to the epsilon-value calculated in 1. count it as hit
 * }
 *
 * Parameter description:
 *  keypoints1 the vector of keypoints that were detected in the first image
 *  keypoints2 the vector of keypoints that were detected in the second image
 *  homography the projective transformation that contains information how to transform a point from the second image back to the first image (so it's the inverted homography from H1toXp)
 *  epsilonDivisor a factor that can be used to restrict the nearest-neighbour-search area. E.g. 2 limits the search-area in the first image to half the euclidean distance to the closest neighbor in the second image.
 */
double evaluateMatching (vector<KeyPoint> keypoints1, vector<KeyPoint> keypoints2, Mat homography, int epsilonDivisor) {

	int hits = 0;
	int misses = 0;

	for(vector<KeyPoint>::size_type i = 0; i != keypoints2.size(); i++) {
		// Read the detected Keypoint
		KeyPoint keyPointInSecondImage = keypoints2[i];

		float epsilon = 100000; // Start initially with very high value

		// Get closest other point to determine Epsilon for this point (Distance from nearest neighbour/2)
		for(vector<KeyPoint>::size_type j = 0; j != keypoints2.size(); j++) {
			if(i==j) {
				continue;
			}
			double dist = euclideanDist(keyPointInSecondImage.pt,keypoints2[j].pt) ;
			if(dist < epsilon) {
				epsilon = dist;
			}
		}

		// Half of the distance to the Nearest Neighbour
		float maxEpsilon = epsilon / epsilonDivisor;

		// Apply miminum distance to avoid disqualification due to numerical reasons
		if(maxEpsilon < 2) {
			maxEpsilon = 2;
		}

		// Construct a 3x1 homogenised vector
		Matx31f locationInSecondImage (keyPointInSecondImage.pt.x, keyPointInSecondImage.pt.y, 1);
		// Estimate where the keypoint should be in the original image (by applying the known homography)
		Mat locationInFirstImage = homography * Mat(locationInSecondImage);
		// Get the point and normalise it to back from homogenised coordinate system.
		Point2f projectedPointInFirstImage = Point2f(locationInFirstImage.at<float>(0,0)/locationInFirstImage.at<float>(2,0),locationInFirstImage.at<float>(1,0)/locationInFirstImage.at<float>(2,0));
				
		bool found = false;
		// Check if the point also exists in the first keypoint-set
		for(vector<KeyPoint>::size_type k = 0; k != keypoints1.size(); k++) {
			if(euclideanDist(keypoints1[k].pt, projectedPointInFirstImage) <= maxEpsilon) {
				//cout << "Match found: " << newLoc.x << "-" << projectedKeyPoint.pt.x << " " << newLoc.y << "-" << projectedKeyPoint.pt.y << endl;
				hits++;
				found = true;
				break;
			}
		}
		if(found == false) {
			//cout << "No Match found: " << newLoc.x << " " << newLoc.y << endl;
			misses ++;
		}
	}

	double hitRatio = 100.0f*hits/(misses+hits);
	cout << "\tMatches found: " << hits << "\tMisses:" << misses << "\tRatio:" << hitRatio << endl;

	return hitRatio;
}

/**
 * Helper method that can be used to visually verify the homographies and the keypoint location.
 *
 * It takes all keypoints from an image and the image itself and draws them as green circles into the colour image.
 * Then it takes all points from keypoints2, transforms them with the given homography and draws them as red circles into the colour image.
 * Finally the results will be displayed until the user hits a key.
 * 
 * Parameter description:
 *  image1col the colour image that will be used to draw upon (will be displayed at the end).
 *  keypoints1 the vector of keypoints that were detected in the first image
 *  keypoints2 the vector of keypoints that were detected in the second image
 *  homography the projective transformation that contains information how to transform a point from keypoints2 back to the first image (so it's the inverted homography from H1toXp)
 */
void drawHomographyKeypoints(Mat image1col, vector<KeyPoint> keypoints1, vector<KeyPoint> keypoints2, Mat homography) {
	
	static CvScalar red_color[] ={0,0,255};
	static CvScalar green_color[] ={0,255,0};
			for(vector<KeyPoint>::size_type k = 0; k != keypoints2.size(); k++) {
				// Take each keypoint from the second set of keypoints and transform it to the corresponding points in the first image.
				KeyPoint p = keypoints2[k];
				// Construct a 3x1 vector
				Matx31f location(p.pt.x, p.pt.y,1);
				// Transform it into the position of the first image
				Mat newLocation = homography * Mat(location);
				// Get the point and normalise it to back from homogenised coordinate system.
				Point2f newLoc = Point2f(newLocation.at<float>(0,0)/newLocation.at<float>(2,0),newLocation.at<float>(1,0)/newLocation.at<float>(2,0));
				
				CvPoint center;
				int radius = 3;
				center.x = cvRound(newLoc.x);
				center.y = cvRound(newLoc.y);
				circle( image1col , center, radius, red_color[0], 1, 8, 0 );
			}

			for(vector<KeyPoint>::size_type k = 0; k != keypoints1.size(); k++) {
				// Take each keypoint and just draw it onto the image
				KeyPoint p = keypoints1[k];

				CvPoint center;
				int radius = 3;
				center.x = cvRound(p.pt.x);
				center.y = cvRound(p.pt.y);
				circle( image1col , center, radius, green_color[0]);
			}
			
			cvNamedWindow("Image with keypoints", CV_WINDOW_AUTOSIZE); // creating a new window each time        
			imshow("Image with keypoints", image1col );	
			cvWaitKey(0);
}

void runFeatureDetectorEvaluation( )
{
	// Path to the testdata-directory
#ifdef __APPLE__
	string testdataDirectory = "/Users/Head/Dropbox/Performance Evaluation Paper/Testdata/";
#else
	string testdataDirectory = "../../testdata/";
#endif

	// Initialise the test-data

	// Specify the size of the testFolders-array
	const int numberOfScenes = 10;
	const int picturesPerScene = 6;
	// These are the folders that must exist in testdataDirectory in order to run the evaluation
	string testFolders[numberOfScenes]  = {"Bark", "Bikes", "Boat", "Christchurch", "Graf", "Leuven","Memorial", "Trees", "Ubc", "Wall"};
	// Path to all individual images (8 folders/scenes with each 6 images, so 48 images in total)
	vector<vector<string> > testFiles;
	// Path to all homography-files (8 folders/scene with 5 transformed images in each folder)
	vector<vector<string> > homographyFiles;

	// Construct the file-paths
	for(int i=0; i<numberOfScenes; i++)  {
		// Construct the paths to the images
		testFiles.push_back(vector<string>());
		for(int j = 1; j<=6; j++) {
			testFiles[i].push_back(testdataDirectory + testFolders[i] + "/img" + IntToString(j) + ".png");
		}
		// Construct the paths to the homographies
		homographyFiles.push_back(vector<string>());
		for(int j = 2; j<=6; j++) {
			homographyFiles[i].push_back(testdataDirectory + testFolders[i] + "/H1to" + IntToString(j) + "p");
		}
	}

	// Specify the number of detectors
	const int numberOfDetectors = 11;
	// "FAST" � FastFeatureDetector 
	// "STAR" � StarFeatureDetector 
	// "SIFT" � SIFT (nonfree module) 
	// "SURF" � SURF (nonfree module) 
	// "ORB" � ORB 
	// "BRISK" � BRISK 
	// "MSER" � MSER 
	// "GFTT" � GoodFeaturesToTrackDetector 
	// "HARRIS" � GoodFeaturesToTrackDetector with Harris detector enabled 
	// "Dense" � DenseFeatureDetector 
	// "SimpleBlob" � SimpleBlobDetector 
	string featureDetectors[numberOfDetectors]  = {"FAST", "STAR", "SIFT", "SURF", "ORB", "BRISK", "MSER", "GFTT", "HARRIS", "Dense", "SimpleBlob"};
		
	// Start the total clock that will measure the overall-time for everything
	clock_t tTotalStart = clock();

	// Iterate over all different detectors
	for(int l=0; l<numberOfDetectors; l++) {

		cout << endl << "Running performance evaluation with " << featureDetectors[l] << endl;

		clock_t tStart = clock();
		int numberOfFeaturesDetected = 0;
		double percentageOfFeaturesDetectedEpsilonUnbounded = 0, percentageOfFeaturesDetectedEpsilonBounded = 0, percentageOfFeaturesDetectedHalfEpsilonUnbounded = 0, percentageOfFeaturesDetectedHalfEpsilonBounded = 0;

		Ptr<FeatureDetector> detector;
		detector = FeatureDetector::create(featureDetectors[l]);
	
		// Evaluate performance
		for(int i=0; i<numberOfScenes; i++)  {
			// Load reference image
			Mat image1 = imread(testFiles[i][0], CV_LOAD_IMAGE_GRAYSCALE);
			// Output the currently evaluated scene
			cout << endl << "Scene: " << testFolders[i] << endl;

			// creating a new window for each scene and display the scene
			//cvNamedWindow(testFiles[i][0].c_str(), CV_WINDOW_AUTOSIZE);         
			//imshow( testFiles[i][0].c_str(), image1 );	

			// Calculate keypoints
			vector<KeyPoint> image1Keypoints, bestImage1Keypoints;
			detector->detect(image1, image1Keypoints);
			sort(image1Keypoints.begin(),image1Keypoints.end(), keypointComparer);
			if(image1Keypoints.size() > 500) {
				// Take the first 500 elements, which are the best keypoints according to their response
				bestImage1Keypoints = vector<KeyPoint>(image1Keypoints.begin(),image1Keypoints.begin()+500); 
			}
			std::cout << "Number of "<< featureDetectors[l] << " Keypoints: " << image1Keypoints.size() << std::endl;
			numberOfFeaturesDetected += image1Keypoints.size();

			for(int j=1; j<6; j++) {
				// Load transformed images
				Mat image2 = imread(testFiles[i][j], CV_LOAD_IMAGE_GRAYSCALE);
				// Output the currently evaluated scene
				cout << "Comparing to: " << testFiles[i][j] << endl;
				// Get the correct homography
				Mat homography = readHomography(homographyFiles[i][j-1]);
				// Invert the homography to go from the second image to the first image (instead of from the first to the second)
				invert(homography,homography);
				// Output the homography for debugging purposes
				//cout << "M = " << endl << " " << homography << endl << endl;

				// Calculate keypoints
				vector<KeyPoint> image2Keypoints, bestImage2Keypoints;
				detector->detect(image2, image2Keypoints);		
				sort(image2Keypoints.begin(),image2Keypoints.end(), keypointComparer);
				if(image2Keypoints.size() > 500) {
					// Take the first 500 elements, which are the best keypoints according to their response
					bestImage2Keypoints = vector<KeyPoint>(image2Keypoints.begin(),image2Keypoints.begin()+500); 
				}
				std::cout << "Number of "<< featureDetectors[l] << " Keypoints: " << image2Keypoints.size() << std::endl;
				numberOfFeaturesDetected += image2Keypoints.size();

				/*for(int m = 0; m < image2Keypoints.size() && m < 100; m++) {
					std::cout << featureDetectors[l] << " Response "<< m << ": " << image2Keypoints[m].response << std::endl;	
				}*/
				
				//// -----------------------------------
				// Evaluate the keypoints and output the matching rate to the console
				//// -----------------------------------
				percentageOfFeaturesDetectedEpsilonUnbounded += evaluateMatching(image1Keypoints, image2Keypoints, homography, 1);
				percentageOfFeaturesDetectedEpsilonBounded += evaluateMatching(bestImage1Keypoints, bestImage2Keypoints, homography, 1);
				percentageOfFeaturesDetectedHalfEpsilonUnbounded += evaluateMatching(image1Keypoints, image2Keypoints, homography, 2);
				percentageOfFeaturesDetectedHalfEpsilonBounded += evaluateMatching(bestImage1Keypoints, bestImage2Keypoints, homography, 2);

				////// -----------------------------------
				//// The following two lines can be used to visually verify the homographies and the keypoint location
				////// -----------------------------------
				//// Load the original colour-image again for displaying
				//Mat image1col = imread(testFiles[i][0], CV_LOAD_IMAGE_COLOR);
				//// Project the second keypoints and draw the original and the projected keypoints in a single image
				//drawHomographyKeypoints(image1col, image1Keypoints, image2Keypoints, homography);

				////// -----------------------------------
				//// The following lines can be used to visually verify the homographies loaded
				//// Warp the second image and show it next to the original image to verify that the homography is correct
				//// The result should be almost the same picture shown five times (as warped back to the original position) including some blur
				////// -----------------------------------
				//try {
				//	Mat image2Trans;
				//	warpPerspective(image2, image2Trans, homography, image2.size());
				//	cvNamedWindow(testFiles[i][j].c_str(), CV_WINDOW_AUTOSIZE); // creating a new window each time        
				//	imshow( testFiles[i][j].c_str(), image2Trans );	
				//	cvWaitKey(0);
				//} catch (cv::Exception exception) {
				//	std::cout << exception.msg<< std::endl;
				//}
			
			}
		}
		
		cout << featureDetectors[l] << " average percentage of hits (e=1, unbounded): " << ((double)(percentageOfFeaturesDetectedEpsilonUnbounded)/(numberOfScenes*picturesPerScene)) << endl;
		cout << featureDetectors[l] << " average percentage of hits (e=1,   bounded): " << ((double)(percentageOfFeaturesDetectedEpsilonBounded)/(numberOfScenes*picturesPerScene)) << endl;
		cout << featureDetectors[l] << " average percentage of hits (e=2, unbounded): " << ((double)(percentageOfFeaturesDetectedHalfEpsilonUnbounded)/(numberOfScenes*picturesPerScene)) << endl;
		cout << featureDetectors[l] << " average percentage of hits (e=2,   bounded): " << ((double)(percentageOfFeaturesDetectedHalfEpsilonBounded)/(numberOfScenes*picturesPerScene)) << endl;
		cout << featureDetectors[l] << " average number of keypoints: " << ((double)(numberOfFeaturesDetected)/(numberOfScenes*picturesPerScene)) << endl;
		// This time is only useful if no evaluateMatching method is called, otherwise it will measure more than just the time taken to calculate the descriptors
		cout << featureDetectors[l] << " time taken: " << ((double)(clock() - tStart)/CLOCKS_PER_SEC) << "s" << endl;
	}
	
	cout << "Total run-time for detector evaluation: " << ((double)(clock() - tTotalStart)/CLOCKS_PER_SEC) << "s" << endl;
}

