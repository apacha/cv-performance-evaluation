#ifndef FEATUREDETECTOREVALUATION_H
#define FEATUREDETECTOREVALUATION_H

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc_c.h>
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/nonfree/nonfree.hpp"
#include "opencv2/imgproc/imgproc_c.h"
#include "opencv2/legacy/compat.hpp"
#include <opencv2/opencv.hpp>
#include <stdio.h>
#include <time.h>
#include <string>
#include "utils.h"

using namespace cv;
using namespace std;

/** 
 * This application evaluates run-times for different feature descriptors (based on previously detected features).
 */
void runFeatureDetectorEvaluation();

#endif