#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc_c.h>
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/nonfree/nonfree.hpp"
#include "opencv2/imgproc/imgproc_c.h"
#include "opencv2/legacy/compat.hpp"
#include <stdio.h>
#include <iostream>
#include <opencv2/opencv.hpp>
#include "descriptors.h"
#include <time.h>

using namespace cv;

int main( int argc, char** argv )
{
#ifdef __APPLE__
    string filePath = "/Users/Head/Development/TrackingData/naist000.00.00.00.000.jpg";
#else
	string filePath = "../../testdata/img1.png";
#endif

	Mat image;
	image = imread(filePath, CV_LOAD_IMAGE_GRAYSCALE);

	if(!image.data)
	{
		printf( "No image data \n" );
		return -1;
	}

	// Initialise the nonfree-module which is required for SIFT
	cv::initModule_nonfree();

	clock_t tStart = clock();
	calculateSurf(image, false);
	printf("SURF time taken: %.2fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);
	
    tStart = clock();
    calculateSurfOrb(image);
    printf("SURF2 time taken: %.2fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);
    
	tStart = clock();
	calculateSift(image, false);
	printf("SIFT time taken: %.2fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);

	tStart = clock();
	calculateBrisk(image, true);
	printf("BRISK time taken: %.2fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);

	tStart = clock();
	calculateOrb(image, false);
	printf("ORB time taken: %.2fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);

	tStart = clock();
	calculateMser(image);
	printf("MSER time taken: %.2fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);

	tStart = clock();
	std::vector<cv::KeyPoint> fastKeypoints = calculateFast(image);
	printf("FAST time taken: %.2fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);

	tStart = clock();
	calculateShiTomasi (image);
	printf("Shi-Tomasi time taken: %.2fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);

	tStart = clock();
	calculateFreak(image, fastKeypoints);
    printf("FREAK time taken: %.2fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);
	
	//namedWindow( "Display Image", CV_WINDOW_AUTOSIZE );
	imshow( "Display Image", image );

	waitKey(0);

	return 0;
}

