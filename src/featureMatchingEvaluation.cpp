#include "featureMatchingEvaluation.h"

/** 
* This application evaluates run-times for different feature descriptors (based on previously detected features).
*	featureDetector The feature detector that should be used. Any of  {"FAST", "STAR", "SIFT", "SURF", "ORB", "BRISK", "MSER", "GFTT", "HARRIS", "Dense", "SimpleBlob"}
*	featureDescriptor The feature descriptor that should be used. Any of { "SIFT", "SURF", "ORB", "BRISK", "BRIEF", "FREAK" };
*	featureDescriptorNeedsConversion Flag indicating, whether the feature descriptor needs conversion. True, if a binary descriptor is used (e.g. ORB, BRISK, BRIEF, FREAK), false otherwise.
*	useNN Flag indicating whether Nearest Neighborr (true) or k-Nearest Neighbour (false) with k=3 should be performed.
*/
void runFeatureMatchingEvaluation(string featureDetector, string featureDescriptor, bool featureDescriptorsNeedConversion, bool useNN)
{
	// Path to the testdata-directory
#ifdef __APPLE__
	string testdataDirectory = "/Users/Head/Dropbox/Performance Evaluation Paper/Testdata/";
#else
	string testdataDirectory = "../../testdata/";
#endif

	// Initialise the test-data

	// Specify the size of the testFolders-array
	const int numberOfScenes = 10;
	const int picturesPerScene = 6;
	// These are the folders that must exist in testdataDirectory in order to run the evaluation
	string testFolders[numberOfScenes]  = {"Bark", "Bikes", "Boat", "Christchurch", "Graf", "Leuven","Memorial", "Trees", "Ubc", "Wall"};
	// Path to all individual images (8 folders/scenes with each 6 images, so 48 images in total)
	vector<vector<string> > testFiles;
	// Path to all homography-files (8 folders/scene with 5 transformed images in each folder)
	vector<vector<string> > homographyFiles;

	// Construct the file-paths
	for(int i=0; i<numberOfScenes; i++)  {
		// Construct the paths to the images
		testFiles.push_back(vector<string>());
		for(int j = 1; j<=6; j++) {
			testFiles[i].push_back(testdataDirectory + testFolders[i] + "/img" + IntToString(j) + ".png");
		}
		// Construct the paths to the homographies
		homographyFiles.push_back(vector<string>());
		for(int j = 2; j<=6; j++) {
			homographyFiles[i].push_back(testdataDirectory + testFolders[i] + "/H1to" + IntToString(j) + "p");
		}
	}

	// Total number of matchers is 12: 6 different matchers from generic Matcher, 6 configuration for FLANN matcher
	const int numberOfGenericMatchers = 6;
	const int numberOfFlannMatchers = 6;

	// Array of different matching strategies for Generic Matcher
	string genericMatcher[numberOfGenericMatchers] = {"BruteForce", "BruteForce-L1", "BruteForce-Hamming", "BruteForce-Hamming(2)", "BruteForce-SL2", "FlannBased"};
	bool genericNeedConversion[numberOfGenericMatchers] = {false, false, false, false, false, true};
	// Flags, indicating, whether these matchers allow non-binary descriptors
	bool genericAllowsNonBinary[numberOfGenericMatchers] = {true, true, false, false, true, true};

	// The IndexParams-object to initialise the generic FLANN-matcher object
	flann::IndexParams *flannMatcherParams[numberOfFlannMatchers] = {new flann::LinearIndexParams(), new flann::KDTreeIndexParams(), new flann::KMeansIndexParams(), new flann::CompositeIndexParams(), new flann::LshIndexParams(20,10,2), new flann::AutotunedIndexParams()};
	// The names to be displayed
	string flannMatcherNames[numberOfFlannMatchers]  = {"Linear Brute Force", "KD-Tree (4 trees)", "K-Means", "Composite (KD-Tree and K-Means)", "Locality Sensitive Hashing", "Autotuned based on data"};
	// Boolean, indicating if features have to be converted from binary to float. True, if the matching does not allow binary descriptors
	bool flannNeedConversion[numberOfFlannMatchers] = {true, true, true, true, false, true};
	// Flags, indicating, whether these matchers allow non-binary descriptors
	bool flannAllowsNonBinary[numberOfFlannMatchers] = {true, true, true, true, false, true};
	
	// Maximum number of keypoints that are allowed. Very high (> 30000) to allow unbounded number. ORB default is 500
	const int maxNumberOfKeypoints = 500;

	// Initialise the Feature detector and feature descriptor
	Ptr<FeatureDetector> detector = FeatureDetector::create(featureDetector);
	Ptr<DescriptorExtractor> extractor = DescriptorExtractor::create(featureDescriptor);

	// Start the total clock that will measure the overall-time for everything
	clock_t tTotalStart = clock();
	// Total number of features detected
	int numberOfFeaturesDetected = 0;

	// Array to store all the runtime from all matchers. The first 6 elements are for generic matchers, the other 6 are for flann-based matchers
	double runTime[numberOfGenericMatchers + numberOfFlannMatchers];
	for(int i=0; i<numberOfGenericMatchers + numberOfFlannMatchers; i++) {
		runTime[i] = 0;
	}

	// The array for references to all flann-matchers.
	FlannBasedMatcher flannMatcher[numberOfFlannMatchers];
	// Initialise Flann-Matchers
	for(int l=0; l<numberOfFlannMatchers; l++) {
		flannMatcher[l] = FlannBasedMatcher(flannMatcherParams[l]);
	}

	// Evaluate performance
	for(int i=0; i<numberOfScenes; i++)  {
		// Load reference image
		Mat image1 = imread(testFiles[i][0], CV_LOAD_IMAGE_GRAYSCALE);
		// Output the currently evaluated scene
		cout << endl << "Scene: " << testFolders[i] << endl;

		// Calculate keypoints
		vector<KeyPoint> image1Keypoints;
		detector->detect(image1, image1Keypoints);
		sort(image1Keypoints.begin(),image1Keypoints.end(), keypointComparer);
		if(image1Keypoints.size() > maxNumberOfKeypoints) {
			// Take the first 500 elements, which are the best keypoints according to their response
			image1Keypoints = vector<KeyPoint>(image1Keypoints.begin(),image1Keypoints.begin()+maxNumberOfKeypoints); 
		} 

		std::cout << "Number of "<< featureDetector << " Keypoints: " << image1Keypoints.size() << std::endl;
		numberOfFeaturesDetected += image1Keypoints.size();
		// Calculate features
		Mat image1Descriptors, image1DescriptorsFloat;
		extractor->compute(image1, image1Keypoints, image1Descriptors);
		// If the data is binary (e.g. ORB Descriptor) and the algorithm expects floats and does not allow binary, convert the descriptors 
		if(image1Descriptors.type()!=CV_32F && featureDescriptorsNeedConversion) {
			image1Descriptors.convertTo(image1DescriptorsFloat, CV_32F);
		}

		for(int j=1; j<6; j++) {
			// Load transformed images
			Mat image2 = imread(testFiles[i][j], CV_LOAD_IMAGE_GRAYSCALE);
			// Output the currently evaluated scene
			cout << endl << "Comparing to: " << testFiles[i][j] << endl;
			// Get the correct homography
			Mat homography = readHomography(homographyFiles[i][j-1]);
			// Invert the homography to go from the second image to the first image (instead of from the first to the second)
			invert(homography,homography);
			// Output the homography for debugging purposes
			//cout << "M = " << endl << " " << homography << endl << endl;

			// Calculate keypoints
			vector<KeyPoint> image2Keypoints;
			detector->detect(image2, image2Keypoints);	
			sort(image2Keypoints.begin(),image2Keypoints.end(), keypointComparer);
			if(image2Keypoints.size() > maxNumberOfKeypoints) {
				// Take the first 500 elements, which are the best keypoints according to their response
				image2Keypoints = vector<KeyPoint>(image2Keypoints.begin(),image2Keypoints.begin()+maxNumberOfKeypoints); 
			} 	
			std::cout << "Number of "<< featureDetector << " Keypoints: " << image2Keypoints.size() << std::endl;
			numberOfFeaturesDetected += image2Keypoints.size();

			// Calculate features
			Mat image2Descriptors, image2DescriptorsFloat;
			extractor->compute(image2, image2Keypoints, image2Descriptors);
			// If the data is binary (e.g. ORB Descriptor) and the algorithm expects floats and does not allow binary, convert the descriptors 
			if(image2Descriptors.type()!=CV_32F && featureDescriptorsNeedConversion) {
				image2Descriptors.convertTo(image2DescriptorsFloat, CV_32F);
			}

			//// -----------------------------------
			// Evaluate the matching
			//// -----------------------------------

			// Iterate over all different generic matchers
			for(int l=0; l<numberOfGenericMatchers; l++) {
				// featureDescriptorsNeedConversion is true, when the features are binary, so everything is fine, if it is true.
				// genericAllowsNonBinary is true, when non-binary are supported, so everything is fine, if it is true.
				if(!featureDescriptorsNeedConversion && !genericAllowsNonBinary[l]) {
					// Filter the cases, where Hamming-distance is required, but descriptor is non-binary:
					// featureDescriptorsNeedConversion = false (its non-binary) && genericAllowsNonBinary = false (doesn't allow non-binary)
					continue;
				}

				// Run with generic Matcher
				Ptr<DescriptorMatcher> matcher = DescriptorMatcher::create(genericMatcher[l]);
				// Vector for the results
				std::vector<DMatch> nnMatches;
				std::vector<std::vector<DMatch>> knnMatches;
				// Start the clock for matching
				clock_t descriptorStart = clock();
				// Perform matching - Use converted or binary features, depending on what the matcher is expecting
				try{
					if(useNN) {
						// Perform NN-Matching
						if(genericNeedConversion[l] && featureDescriptorsNeedConversion) {
							matcher->match(image2DescriptorsFloat, image1DescriptorsFloat, nnMatches);
						} else {
							matcher->match(image2Descriptors, image1Descriptors, nnMatches);
						}
					} else {
						// Perform kNN-Matching with k=3				
						if(genericNeedConversion[l] && featureDescriptorsNeedConversion) {
							matcher->knnMatch(image2DescriptorsFloat, image1DescriptorsFloat, knnMatches, 3);
						} else {
							matcher->knnMatch(image2Descriptors, image1Descriptors, knnMatches, 3);
						}
					}
				} catch (Exception ex) {
					cout << ex.msg;
				}
				// Stop the clock for matching
				double time = clock() - descriptorStart;
				// Write time to total time
				runTime[l] += time;
				cout << "Running time with " << genericMatcher[l] << ": " << ((double)(time)/CLOCKS_PER_SEC) << "s" <<endl;
			}


			// Iterate over all different flann matchers
			for(int l=0; l<numberOfFlannMatchers; l++) {
				// featureDescriptorsNeedConversion is true, when the features are binary, so everything is fine, if it is true.
				// flannAllowsNonBinary is true, when non-binary are supported, so everything is fine, if it is true.
				if(!featureDescriptorsNeedConversion && !flannAllowsNonBinary[l]) {
					// Filter the cases, where Hamming-distance is required, but descriptor is non-binary:
					// featureDescriptorsNeedConversion = false (its non-binary) && flannAllowsNonBinary = false (doesn't allow non-binary)
					continue;
				}

				// Run FLANN-based matcher (initialisation has happened before)
				// Vector for the results
				std::vector<DMatch> nnMatches;
				std::vector<std::vector<DMatch>> knnMatches;
				// Start the clock for matching
				clock_t descriptorStart = clock();
				// Perform matching - Use converted or binary features, depending on what the matcher is expecting
				try{
					if(useNN) {
						// Perform NN-Matching
						if(flannNeedConversion[l] && featureDescriptorsNeedConversion) {
							flannMatcher[l].match(image2DescriptorsFloat, image1DescriptorsFloat, nnMatches);
						} else {
							flannMatcher[l].match(image2Descriptors, image1Descriptors, nnMatches);
						}
					} else {
						// Perform kNN-Matching with k=3	
						if(flannNeedConversion[l] && featureDescriptorsNeedConversion) {
							flannMatcher[l].knnMatch(image2DescriptorsFloat, image1DescriptorsFloat, knnMatches, 3);
						} else {
							flannMatcher[l].knnMatch(image2Descriptors, image1Descriptors, knnMatches, 3);
						}
					}
				} catch (Exception ex) {
					cout << ex.msg;
				}
				// Stop the clock for matching
				double time = clock() - descriptorStart;
				// Write time to total time
				runTime[l+numberOfGenericMatchers] += time;
				cout << "Running time with " << flannMatcherNames[l] << ": " << ((double)(time)/CLOCKS_PER_SEC) << "s" <<endl;
			}

		}
	}

	// Output summary of all results for the generic matchers
	for(int i=0; i<numberOfGenericMatchers; i++) {
		cout << genericMatcher[i] << " time taken: " << ((double)(runTime[i])/CLOCKS_PER_SEC) << "s" << endl;
	}

	// Output summary of all results for the flann matchers
	for(int i=0; i<numberOfFlannMatchers; i++) {
		cout << flannMatcherNames[i] << " time taken: " << ((double)(runTime[i + numberOfGenericMatchers])/CLOCKS_PER_SEC) << "s" << endl;
	}

	// Output statistics
	cout << "Total average number of keypoints: " << numberOfFeaturesDetected / (numberOfScenes * picturesPerScene) << endl;
	cout << "Total run-time for matching evaluation: " << ((double)(clock() - tTotalStart)/CLOCKS_PER_SEC) << "s" << endl;

	return;
}

