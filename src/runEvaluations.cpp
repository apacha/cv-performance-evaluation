#include <stdio.h>
#include "featureDetectorEvaluation.h"
#include "featureDescriptorEvaluation.h"
#include "featureMatchingEvaluation.h"

/**
 * Initial starting point for running performance evaluations
 */
int main( int argc, char** argv )
{
	//// Perform detector evaluation of different keypoint detectors (FAST, ORB, SIFT, SURF, GFTT,...)
	//runFeatureDetectorEvaluation();

	//// Perform descriptor evaluation of different keypoint descriptors
	//runFeatureDescriptorEvaluation();

	// Perform matching evaluation of different keypoint-matching (FLANN, Brute-Force, LST, ...)
	/*runFeatureMatchingEvaluation("ORB", "ORB", true, true);
	runFeatureMatchingEvaluation("ORB", "ORB", true, false);*/
	runFeatureMatchingEvaluation("SURF", "SURF", false, true);
	runFeatureMatchingEvaluation("SURF", "SURF", false, false);
	//runFeatureMatchingEvaluation("FAST", "FREAK", true, true); // NN
	//runFeatureMatchingEvaluation("FAST", "FREAK", true, false); // kNN
	//runFeatureMatchingEvaluation("FAST", "ORB", true, true); // NN
	//runFeatureMatchingEvaluation("FAST", "ORB", true, false); // kNN

	//// Perform matching quality analysis
	//runFeatureMatchingQualityEvaluation("ORB", "ORB", true);
	//runFeatureMatchingQualityEvaluation("BRISK", "BRISK", true);
	//runFeatureMatchingQualityEvaluation("FAST", "FREAK", true);
	//runFeatureMatchingQualityEvaluation("FAST", "ORB", true);
	//runFeatureMatchingQualityEvaluation("SIFT", "SIFT", false);
	//runFeatureMatchingQualityEvaluation("SURF", "SURF", false);

	return 0;
}

