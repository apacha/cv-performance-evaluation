#ifndef DESCRIPTORS_H
#define DESCRIPTORS_H

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc_c.h>
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/nonfree/nonfree.hpp"
#include "opencv2/imgproc/imgproc_c.h"
#include "opencv2/legacy/compat.hpp"
#include <stdio.h>
#include <iostream>
#include <vector>
#include <opencv2/opencv.hpp>

using namespace cv;

/**
 * Keypoint detectors and feature descriptors 
 */

void calculateSurf (Mat image, bool calculateDescriptors = false);
void calculateSurfOrb (Mat image);
void calculateSift (Mat image, bool calculateDescriptors = false);
void calculateBrisk (Mat image, bool calculateDescriptors = false);
void calculateOrb (Mat image, bool calculateDescriptors = false);

/**
 * Keypoint detectors 
 */
void calculateMser (Mat image);
std::vector<cv::KeyPoint> calculateFast (Mat image);
std::vector<Point2f> calculateShiTomasi (Mat image);
 
/**
 * Feature descriptors 
 */
void calculateFreak (Mat image, std::vector<cv::KeyPoint> freakImageKeypoints);

#endif