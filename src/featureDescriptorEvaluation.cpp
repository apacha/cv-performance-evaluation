#include "featureDescriptorEvaluation.h"

/** 
* This application evaluates run-times for different feature descriptors (based on previously detected features.
*/
void runFeatureDescriptorEvaluation( )
{
	// Path to the testdata-directory
#ifdef __APPLE__
	string testdataDirectory = "/Users/Head/Dropbox/Performance Evaluation Paper/Testdata/";
#else
	string testdataDirectory = "../../testdata/";
#endif

	// Initialise the test-data

	// Specify the size of the testFolders-array
	const int numberOfScenes = 10;
	const int picturesPerScene = 6;
	// These are the folders that must exist in testdataDirectory in order to run the evaluation
	string testFolders[numberOfScenes]  = {"Bark", "Bikes", "Boat", "Christchurch", "Graf", "Leuven","Memorial", "Trees", "Ubc", "Wall"};
	// Path to all individual images (8 folders/scenes with each 6 images, so 48 images in total)
	vector<vector<string> > testFiles;

	// Construct the file-paths
	for(int i=0; i<numberOfScenes; i++)  {
		// Construct the paths to the images
		testFiles.push_back(vector<string>());
		for(int j = 1; j<=6; j++) {
			testFiles[i].push_back(testdataDirectory + testFolders[i] + "/img" + IntToString(j) + ".png");
		}
	}

	// The current feature detector (used to find the keypoints)
	// Using SURF keypoints, because they are already ordered by their response (how "good" they are) and a unbounded number of features (unlike ORB).
	string featureDetector = "SURF"; // Any of { "SIFT", "SURF", "ORB", "BRISK", "MSER", "GFTT", "HARRIS" }

	// Specify the number of descriptors
	const int numberOfDescriptors = 6;
	string featureDescriptors[numberOfDescriptors]  = { "SIFT", "SURF", "ORB", "BRISK", "BRIEF", "FREAK" };

	Ptr<FeatureDetector> detector = FeatureDetector::create(featureDetector);

	// Start the total clock that will measure the overall-time for everything
	clock_t tTotalStart = clock();

	double runTime[numberOfDescriptors];
	for(int i=0; i<numberOfDescriptors; i++) {
		runTime[i] = 0;
	}

	// Keep track of the total number of keypoints from all images
	double numberOfKeypoints = 0;

	// Evaluate performance
	for(int i=0; i<numberOfScenes; i++)  {
		// Output the currently evaluated scene
		cout << endl << "Scene: " << testFolders[i] << endl;

		for(int j=0; j<6; j++) {
			// Load reference image
			Mat image = imread(testFiles[i][j], CV_LOAD_IMAGE_GRAYSCALE);

			// Calculate keypoints (untimed)
			vector<KeyPoint> imageKeypoints;
			detector->detect(image, imageKeypoints);
			cout << "Image " << j+1 << ", Number of "<< featureDetector << " Keypoints: " << imageKeypoints.size() << std::endl;
			numberOfKeypoints += imageKeypoints.size();

			// Iterate over all different descriptors
			for(int k=0; k<numberOfDescriptors; k++) {
				cout << "Running performance evaluation with " << featureDescriptors[k] << ": ";

				// Start clock
				clock_t tStart = clock();

				// Calculate features
				Mat imageDescriptors;
				Ptr<DescriptorExtractor> extractor = DescriptorExtractor::create(featureDescriptors[k]);
				extractor->compute(image, imageKeypoints, imageDescriptors);

				// Stop clock and take time
				runTime[k] += (double)clock()-tStart;
				cout << (runTime[k]/CLOCKS_PER_SEC) << "s" << endl;
			}
		}
	}

	for(int i=0; i<numberOfDescriptors; i++) {
		cout << featureDescriptors[i] << " time taken: " << ((double)(runTime[i])/CLOCKS_PER_SEC) << "s" << endl;
	}

	cout << "Total average number of keypoints: " << numberOfKeypoints / (numberOfScenes * picturesPerScene) << endl;
	cout << "Total run-time for descriptor evaluation: " << ((double)(clock() - tTotalStart)/CLOCKS_PER_SEC) << "s" << endl;
}