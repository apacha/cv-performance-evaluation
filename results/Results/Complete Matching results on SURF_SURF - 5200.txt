
Scene: Bark
Number of SURF Keypoints: 3634

Comparing to: ../../testdata/Bark/img2.png
Number of SURF Keypoints: 3540
Running time with BruteForce: 0.945s
Running time with BruteForce-L1: 0.877s
Running time with BruteForce-SL2: 0.886s
Running time with FlannBased: 2.656s
Running time with Linear Brute Force: 2.608s
Running time with KD-Tree (4 trees): 2.5s
Running time with K-Means: 1.927s
Running time with Composite (KD-Tree and K-Means): 1.839s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 44.782s

Comparing to: ../../testdata/Bark/img3.png
Number of SURF Keypoints: 3653
Running time with BruteForce: 1.033s
Running time with BruteForce-L1: 0.926s
Running time with BruteForce-SL2: 0.908s
Running time with FlannBased: 2.608s
Running time with Linear Brute Force: 2.591s
Running time with KD-Tree (4 trees): 2.571s
Running time with K-Means: 1.99s
Running time with Composite (KD-Tree and K-Means): 2.007s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 48.757s

Comparing to: ../../testdata/Bark/img4.png
Number of SURF Keypoints: 4344
Running time with BruteForce: 1.262s
Running time with BruteForce-L1: 1.213s
Running time with BruteForce-SL2: 1.075s
Running time with FlannBased: 3.079s
Running time with Linear Brute Force: 3.1s
Running time with KD-Tree (4 trees): 3.476s
Running time with K-Means: 2.376s
Running time with Composite (KD-Tree and K-Means): 2.687s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 42.655s

Comparing to: ../../testdata/Bark/img5.png
Number of SURF Keypoints: 4167
Running time with BruteForce: 1.136s
Running time with BruteForce-L1: 1.025s
Running time with BruteForce-SL2: 1.027s
Running time with FlannBased: 2.958s
Running time with Linear Brute Force: 2.938s
Running time with KD-Tree (4 trees): 3.035s
Running time with K-Means: 2.762s
Running time with Composite (KD-Tree and K-Means): 2.172s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 43.761s

Comparing to: ../../testdata/Bark/img6.png
Number of SURF Keypoints: 4476
Running time with BruteForce: 1.23s
Running time with BruteForce-L1: 1.099s
Running time with BruteForce-SL2: 1.091s
Running time with FlannBased: 3.158s
Running time with Linear Brute Force: 3.151s
Running time with KD-Tree (4 trees): 3.157s
Running time with K-Means: 2.39s
Running time with Composite (KD-Tree and K-Means): 2.333s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 41.242s

Scene: Bikes
Number of SURF Keypoints: 5780

Comparing to: ../../testdata/Bikes/img2.png
Number of SURF Keypoints: 3791
Running time with BruteForce: 1.608s
Running time with BruteForce-L1: 1.501s
Running time with BruteForce-SL2: 1.505s
Running time with FlannBased: 2.404s
Running time with Linear Brute Force: 4.182s
Running time with KD-Tree (4 trees): 2.404s
Running time with K-Means: 2.228s
Running time with Composite (KD-Tree and K-Means): 2.29s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 44.849s

Comparing to: ../../testdata/Bikes/img3.png
Number of SURF Keypoints: 3343
Running time with BruteForce: 1.424s
Running time with BruteForce-L1: 1.337s
Running time with BruteForce-SL2: 1.318s
Running time with FlannBased: 2.22s
Running time with Linear Brute Force: 3.721s
Running time with KD-Tree (4 trees): 2.227s
Running time with K-Means: 2.022s
Running time with Composite (KD-Tree and K-Means): 1.845s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 51.13s

Comparing to: ../../testdata/Bikes/img4.png
Number of SURF Keypoints: 2619
Running time with BruteForce: 1.138s
Running time with BruteForce-L1: 1.074s
Running time with BruteForce-SL2: 1.055s
Running time with FlannBased: 1.866s
Running time with Linear Brute Force: 2.88s
Running time with KD-Tree (4 trees): 1.86s
Running time with K-Means: 1.881s
Running time with Composite (KD-Tree and K-Means): 1.732s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 45.621s

Comparing to: ../../testdata/Bikes/img5.png
Number of SURF Keypoints: 2045
Running time with BruteForce: 0.898s
Running time with BruteForce-L1: 0.835s
Running time with BruteForce-SL2: 0.842s
Running time with FlannBased: 1.554s
Running time with Linear Brute Force: 2.231s
Running time with KD-Tree (4 trees): 1.51s
Running time with K-Means: 1.352s
Running time with Composite (KD-Tree and K-Means): 1.402s
algorithm : 2
branching : 32
centers_init : 0
iterations : 5
checks : 43
eps : 0
sorted : 1
Running time with Autotuned based on data: 61.739s

Comparing to: ../../testdata/Bikes/img6.png
Number of SURF Keypoints: 1573
Running time with BruteForce: 0.681s
Running time with BruteForce-L1: 0.64s
Running time with BruteForce-SL2: 0.642s
Running time with FlannBased: 1.227s
Running time with Linear Brute Force: 1.715s
Running time with KD-Tree (4 trees): 1.212s
Running time with K-Means: 1.189s
Running time with Composite (KD-Tree and K-Means): 1.253s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 47.109s

Scene: Boat
Number of SURF Keypoints: 6737

Comparing to: ../../testdata/Boat/img2.png
Number of SURF Keypoints: 7200
Running time with BruteForce: 3.769s
Running time with BruteForce-L1: 3.613s
Running time with BruteForce-SL2: 3.538s
Running time with FlannBased: 5.214s
Running time with Linear Brute Force: 9.29s
Running time with KD-Tree (4 trees): 5.264s
Running time with K-Means: 3.186s
Running time with Composite (KD-Tree and K-Means): 3.281s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 63.874s

Comparing to: ../../testdata/Boat/img3.png
Number of SURF Keypoints: 5519
Running time with BruteForce: 2.927s
Running time with BruteForce-L1: 2.745s
Running time with BruteForce-SL2: 2.756s
Running time with FlannBased: 4.125s
Running time with Linear Brute Force: 7.027s
Running time with KD-Tree (4 trees): 4.12s
Running time with K-Means: 2.573s
Running time with Composite (KD-Tree and K-Means): 2.581s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 57.866s

Comparing to: ../../testdata/Boat/img4.png
Number of SURF Keypoints: 4591
Running time with BruteForce: 2.441s
Running time with BruteForce-L1: 2.278s
Running time with BruteForce-SL2: 2.295s
Running time with FlannBased: 3.491s
Running time with Linear Brute Force: 6.003s
Running time with KD-Tree (4 trees): 3.659s
Running time with K-Means: 2.423s
Running time with Composite (KD-Tree and K-Means): 2.301s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 55.82s

Comparing to: ../../testdata/Boat/img5.png
Number of SURF Keypoints: 4100
Running time with BruteForce: 2.143s
Running time with BruteForce-L1: 2.019s
Running time with BruteForce-SL2: 2.032s
Running time with FlannBased: 3.068s
Running time with Linear Brute Force: 5.311s
Running time with KD-Tree (4 trees): 3.119s
Running time with K-Means: 2.124s
Running time with Composite (KD-Tree and K-Means): 2.07s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 58.378s

Comparing to: ../../testdata/Boat/img6.png
Number of SURF Keypoints: 3665
Running time with BruteForce: 1.938s
Running time with BruteForce-L1: 1.81s
Running time with BruteForce-SL2: 1.811s
Running time with FlannBased: 2.846s
Running time with Linear Brute Force: 4.654s
Running time with KD-Tree (4 trees): 2.847s
Running time with K-Means: 2.015s
Running time with Composite (KD-Tree and K-Means): 1.964s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 55.654s

Scene: Christchurch
Number of SURF Keypoints: 4828

Comparing to: ../../testdata/Christchurch/img2.png
Number of SURF Keypoints: 5154
Running time with BruteForce: 1.972s
Running time with BruteForce-L1: 1.85s
Running time with BruteForce-SL2: 1.832s
Running time with FlannBased: 3.001s
Running time with Linear Brute Force: 4.8s
Running time with KD-Tree (4 trees): 2.973s
Running time with K-Means: 3.061s
Running time with Composite (KD-Tree and K-Means): 3.312s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 48.166s

Comparing to: ../../testdata/Christchurch/img3.png
Number of SURF Keypoints: 5522
Running time with BruteForce: 2.122s
Running time with BruteForce-L1: 2.058s
Running time with BruteForce-SL2: 2.237s
Running time with FlannBased: 3.895s
Running time with Linear Brute Force: 5.54s
Running time with KD-Tree (4 trees): 3.696s
Running time with K-Means: 3.138s
Running time with Composite (KD-Tree and K-Means): 3.39s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 50.819s

Comparing to: ../../testdata/Christchurch/img4.png
Number of SURF Keypoints: 4771
Running time with BruteForce: 1.97s
Running time with BruteForce-L1: 1.811s
Running time with BruteForce-SL2: 1.784s
Running time with FlannBased: 3.284s
Running time with Linear Brute Force: 4.49s
Running time with KD-Tree (4 trees): 3.162s
Running time with K-Means: 2.405s
Running time with Composite (KD-Tree and K-Means): 2.472s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 49.512s

Comparing to: ../../testdata/Christchurch/img5.png
Number of SURF Keypoints: 4560
Running time with BruteForce: 2.078s
Running time with BruteForce-L1: 1.921s
Running time with BruteForce-SL2: 1.884s
Running time with FlannBased: 3.289s
Running time with Linear Brute Force: 5.231s
Running time with KD-Tree (4 trees): 3.073s
Running time with K-Means: 2.355s
Running time with Composite (KD-Tree and K-Means): 2.307s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 45.234s

Comparing to: ../../testdata/Christchurch/img6.png
Number of SURF Keypoints: 5507
Running time with BruteForce: 2.201s
Running time with BruteForce-L1: 2.109s
Running time with BruteForce-SL2: 2.048s
Running time with FlannBased: 3.492s
Running time with Linear Brute Force: 5.097s
Running time with KD-Tree (4 trees): 3.49s
Running time with K-Means: 2.999s
Running time with Composite (KD-Tree and K-Means): 2.934s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 48.59s

Scene: Graf
Number of SURF Keypoints: 4805

Comparing to: ../../testdata/Graf/img2.png
Number of SURF Keypoints: 5007
Running time with BruteForce: 1.968s
Running time with BruteForce-L1: 1.855s
Running time with BruteForce-SL2: 1.864s
Running time with FlannBased: 3.348s
Running time with Linear Brute Force: 4.649s
Running time with KD-Tree (4 trees): 3.379s
Running time with K-Means: 2.505s
Running time with Composite (KD-Tree and K-Means): 2.416s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 44.051s

Comparing to: ../../testdata/Graf/img3.png
Number of SURF Keypoints: 5602
Running time with BruteForce: 2.209s
Running time with BruteForce-L1: 2.22s
Running time with BruteForce-SL2: 2.12s
Running time with FlannBased: 3.866s
Running time with Linear Brute Force: 5.223s
Running time with KD-Tree (4 trees): 3.838s
Running time with K-Means: 2.557s
Running time with Composite (KD-Tree and K-Means): 2.565s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 43.296s

Comparing to: ../../testdata/Graf/img4.png
Number of SURF Keypoints: 5331
Running time with BruteForce: 2.103s
Running time with BruteForce-L1: 1.964s
Running time with BruteForce-SL2: 1.981s
Running time with FlannBased: 3.672s
Running time with Linear Brute Force: 4.938s
Running time with KD-Tree (4 trees): 3.75s
Running time with K-Means: 2.48s
Running time with Composite (KD-Tree and K-Means): 2.518s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 46.653s

Comparing to: ../../testdata/Graf/img5.png
Number of SURF Keypoints: 5965
Running time with BruteForce: 2.452s
Running time with BruteForce-L1: 2.237s
Running time with BruteForce-SL2: 2.225s
Running time with FlannBased: 4.32s
Running time with Linear Brute Force: 5.558s
Running time with KD-Tree (4 trees): 4.126s
Running time with K-Means: 2.644s
Running time with Composite (KD-Tree and K-Means): 2.69s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 43.899s

Comparing to: ../../testdata/Graf/img6.png
Number of SURF Keypoints: 5941
Running time with BruteForce: 2.349s
Running time with BruteForce-L1: 2.209s
Running time with BruteForce-SL2: 2.191s
Running time with FlannBased: 4.115s
Running time with Linear Brute Force: 5.497s
Running time with KD-Tree (4 trees): 4.184s
Running time with K-Means: 2.747s
Running time with Composite (KD-Tree and K-Means): 2.616s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 42.859s

Scene: Leuven
Number of SURF Keypoints: 5152

Comparing to: ../../testdata/Leuven/img2.png
Number of SURF Keypoints: 4349
Running time with BruteForce: 1.821s
Running time with BruteForce-L1: 1.703s
Running time with BruteForce-SL2: 1.722s
Running time with FlannBased: 2.588s
Running time with Linear Brute Force: 4.376s
Running time with KD-Tree (4 trees): 2.545s
Running time with K-Means: 2.952s
Running time with Composite (KD-Tree and K-Means): 2.915s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 43.429s

Comparing to: ../../testdata/Leuven/img3.png
Number of SURF Keypoints: 3754
Running time with BruteForce: 1.622s
Running time with BruteForce-L1: 1.511s
Running time with BruteForce-SL2: 1.511s
Running time with FlannBased: 2.383s
Running time with Linear Brute Force: 3.814s
Running time with KD-Tree (4 trees): 2.368s
Running time with K-Means: 2.21s
Running time with Composite (KD-Tree and K-Means): 2.007s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 43.102s

Comparing to: ../../testdata/Leuven/img4.png
Number of SURF Keypoints: 3257
Running time with BruteForce: 1.437s
Running time with BruteForce-L1: 1.309s
Running time with BruteForce-SL2: 1.302s
Running time with FlannBased: 2.146s
Running time with Linear Brute Force: 3.275s
Running time with KD-Tree (4 trees): 2.142s
Running time with K-Means: 1.908s
Running time with Composite (KD-Tree and K-Means): 1.849s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 44.903s

Comparing to: ../../testdata/Leuven/img5.png
Number of SURF Keypoints: 2791
Running time with BruteForce: 1.202s
Running time with BruteForce-L1: 1.126s
Running time with BruteForce-SL2: 1.119s
Running time with FlannBased: 1.881s
Running time with Linear Brute Force: 2.799s
Running time with KD-Tree (4 trees): 1.886s
Running time with K-Means: 1.668s
Running time with Composite (KD-Tree and K-Means): 1.702s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 39.878s

Comparing to: ../../testdata/Leuven/img6.png
Number of SURF Keypoints: 2228
Running time with BruteForce: 0.972s
Running time with BruteForce-L1: 0.889s
Running time with BruteForce-SL2: 0.89s
Running time with FlannBased: 1.575s
Running time with Linear Brute Force: 2.235s
Running time with KD-Tree (4 trees): 1.58s
Running time with K-Means: 1.463s
Running time with Composite (KD-Tree and K-Means): 1.437s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 45.592s

Scene: Memorial
Number of SURF Keypoints: 4118

Comparing to: ../../testdata/Memorial/img2.png
Number of SURF Keypoints: 4278
Running time with BruteForce: 1.467s
Running time with BruteForce-L1: 1.368s
Running time with BruteForce-SL2: 1.366s
Running time with FlannBased: 2.432s
Running time with Linear Brute Force: 3.453s
Running time with KD-Tree (4 trees): 2.451s
Running time with K-Means: 2.898s
Running time with Composite (KD-Tree and K-Means): 2.942s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 41.102s

Comparing to: ../../testdata/Memorial/img3.png
Number of SURF Keypoints: 4467
Running time with BruteForce: 1.667s
Running time with BruteForce-L1: 1.456s
Running time with BruteForce-SL2: 1.44s
Running time with FlannBased: 2.77s
Running time with Linear Brute Force: 3.607s
Running time with KD-Tree (4 trees): 2.782s
Running time with K-Means: 2.862s
Running time with Composite (KD-Tree and K-Means): 2.736s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 44.742s

Comparing to: ../../testdata/Memorial/img4.png
Number of SURF Keypoints: 4495
Running time with BruteForce: 1.546s
Running time with BruteForce-L1: 1.439s
Running time with BruteForce-SL2: 1.414s
Running time with FlannBased: 2.925s
Running time with Linear Brute Force: 3.641s
Running time with KD-Tree (4 trees): 2.893s
Running time with K-Means: 2.728s
Running time with Composite (KD-Tree and K-Means): 2.631s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 45.041s

Comparing to: ../../testdata/Memorial/img5.png
Number of SURF Keypoints: 4322
Running time with BruteForce: 1.504s
Running time with BruteForce-L1: 1.39s
Running time with BruteForce-SL2: 1.398s
Running time with FlannBased: 2.761s
Running time with Linear Brute Force: 3.518s
Running time with KD-Tree (4 trees): 2.756s
Running time with K-Means: 2.575s
Running time with Composite (KD-Tree and K-Means): 2.705s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 40.113s

Comparing to: ../../testdata/Memorial/img6.png
Number of SURF Keypoints: 4543
Running time with BruteForce: 1.561s
Running time with BruteForce-L1: 1.483s
Running time with BruteForce-SL2: 1.461s
Running time with FlannBased: 2.846s
Running time with Linear Brute Force: 3.9s
Running time with KD-Tree (4 trees): 2.88s
Running time with K-Means: 3.154s
Running time with Composite (KD-Tree and K-Means): 2.599s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 43.755s

Scene: Trees
Number of SURF Keypoints: 9055

Comparing to: ../../testdata/Trees/img2.png
Number of SURF Keypoints: 9852
Running time with BruteForce: 8.239s
Running time with BruteForce-L1: 8.073s
Running time with BruteForce-SL2: 7.706s
Running time with FlannBased: 8.373s
Running time with Linear Brute Force: 18.792s
Running time with KD-Tree (4 trees): 8.464s
Running time with K-Means: 4.39s
Running time with Composite (KD-Tree and K-Means): 4.435s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 73.874s

Comparing to: ../../testdata/Trees/img3.png
Number of SURF Keypoints: 8492
Running time with BruteForce: 7.092s
Running time with BruteForce-L1: 7.086s
Running time with BruteForce-SL2: 7.101s
Running time with FlannBased: 7.181s
Running time with Linear Brute Force: 15.655s
Running time with KD-Tree (4 trees): 7.174s
Running time with K-Means: 3.879s
Running time with Composite (KD-Tree and K-Means): 4.007s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 72.205s

Comparing to: ../../testdata/Trees/img4.png
Number of SURF Keypoints: 7161
Running time with BruteForce: 6.805s
Running time with BruteForce-L1: 5.692s
Running time with BruteForce-SL2: 5.392s
Running time with FlannBased: 6.494s
Running time with Linear Brute Force: 13.365s
Running time with KD-Tree (4 trees): 5.793s
Running time with K-Means: 3.242s
Running time with Composite (KD-Tree and K-Means): 3.282s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 66.94s

Comparing to: ../../testdata/Trees/img5.png
Number of SURF Keypoints: 6728
Running time with BruteForce: 6.354s
Running time with BruteForce-L1: 5.693s
Running time with BruteForce-SL2: 5.222s
Running time with FlannBased: 5.58s
Running time with Linear Brute Force: 11.74s
Running time with KD-Tree (4 trees): 5.384s
Running time with K-Means: 3.105s
Running time with Composite (KD-Tree and K-Means): 3.086s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 69.707s

Comparing to: ../../testdata/Trees/img6.png
Number of SURF Keypoints: 5258
Running time with BruteForce: 4.471s
Running time with BruteForce-L1: 4.13s
Running time with BruteForce-SL2: 4.094s
Running time with FlannBased: 4.333s
Running time with Linear Brute Force: 9.054s
Running time with KD-Tree (4 trees): 4.425s
Running time with K-Means: 2.655s
Running time with Composite (KD-Tree and K-Means): 2.628s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 63.837s

Scene: Ubc
Number of SURF Keypoints: 6082

Comparing to: ../../testdata/Ubc/img2.png
Number of SURF Keypoints: 6010
Running time with BruteForce: 3.373s
Running time with BruteForce-L1: 3.163s
Running time with BruteForce-SL2: 3.554s
Running time with FlannBased: 3.815s
Running time with Linear Brute Force: 7.183s
Running time with KD-Tree (4 trees): 3.387s
Running time with K-Means: 4.158s
Running time with Composite (KD-Tree and K-Means): 4.152s
algorithm : 2
branching : 32
centers_init : 0
iterations : 10
checks : 56
eps : 0
sorted : 1
Running time with Autotuned based on data: 69.561s

Comparing to: ../../testdata/Ubc/img3.png
Number of SURF Keypoints: 5875
Running time with BruteForce: 3.223s
Running time with BruteForce-L1: 3.044s
Running time with BruteForce-SL2: 3.022s
Running time with FlannBased: 3.745s
Running time with Linear Brute Force: 7.195s
Running time with KD-Tree (4 trees): 3.696s
Running time with K-Means: 3.597s
Running time with Composite (KD-Tree and K-Means): 3.759s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 54.389s

Comparing to: ../../testdata/Ubc/img4.png
Number of SURF Keypoints: 5526
Running time with BruteForce: 3.09s
Running time with BruteForce-L1: 2.906s
Running time with BruteForce-SL2: 2.819s
Running time with FlannBased: 4.138s
Running time with Linear Brute Force: 8.065s
Running time with KD-Tree (4 trees): 4.306s
Running time with K-Means: 3.565s
Running time with Composite (KD-Tree and K-Means): 3.231s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 51.566s

Comparing to: ../../testdata/Ubc/img5.png
Number of SURF Keypoints: 4923
Running time with BruteForce: 2.718s
Running time with BruteForce-L1: 2.541s
Running time with BruteForce-SL2: 2.512s
Running time with FlannBased: 3.607s
Running time with Linear Brute Force: 5.786s
Running time with KD-Tree (4 trees): 3.607s
Running time with K-Means: 2.399s
Running time with Composite (KD-Tree and K-Means): 2.407s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 51.923s

Comparing to: ../../testdata/Ubc/img6.png
Number of SURF Keypoints: 3782
Running time with BruteForce: 2.137s
Running time with BruteForce-L1: 1.96s
Running time with BruteForce-SL2: 1.926s
Running time with FlannBased: 2.852s
Running time with Linear Brute Force: 4.453s
Running time with KD-Tree (4 trees): 2.905s
Running time with K-Means: 2.14s
Running time with Composite (KD-Tree and K-Means): 2.118s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 50.025s

Scene: Wall
Number of SURF Keypoints: 10401

Comparing to: ../../testdata/Wall/img2.png
Number of SURF Keypoints: 8252
Running time with BruteForce: 7.696s
Running time with BruteForce-L1: 7.278s
Running time with BruteForce-SL2: 7.436s
Running time with FlannBased: 6.594s
Running time with Linear Brute Force: 17.847s
Running time with KD-Tree (4 trees): 6.56s
Running time with K-Means: 3.821s
Running time with Composite (KD-Tree and K-Means): 4.445s
algorithm : 2
branching : 16
centers_init : 0
iterations : 5
checks : 74
eps : 0
sorted : 1
Running time with Autotuned based on data: 127.031s

Comparing to: ../../testdata/Wall/img3.png
Number of SURF Keypoints: 7986
Running time with BruteForce: 7.536s
Running time with BruteForce-L1: 7.678s
Running time with BruteForce-SL2: 8.12s
Running time with FlannBased: 8.761s
Running time with Linear Brute Force: 16.929s
Running time with KD-Tree (4 trees): 6.533s
Running time with K-Means: 4.208s
Running time with Composite (KD-Tree and K-Means): 4.348s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 84.788s

Comparing to: ../../testdata/Wall/img4.png
Number of SURF Keypoints: 7921
Running time with BruteForce: 8.086s
Running time with BruteForce-L1: 7.915s
Running time with BruteForce-SL2: 7.418s
Running time with FlannBased: 6.71s
Running time with Linear Brute Force: 15.958s
Running time with KD-Tree (4 trees): 6.777s
Running time with K-Means: 3.738s
Running time with Composite (KD-Tree and K-Means): 4.007s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 93.88s

Comparing to: ../../testdata/Wall/img5.png
Number of SURF Keypoints: 7854
Running time with BruteForce: 9.068s
Running time with BruteForce-L1: 8.25s
Running time with BruteForce-SL2: 8.23s
Running time with FlannBased: 7.938s
Running time with Linear Brute Force: 17.843s
Running time with KD-Tree (4 trees): 7.585s
Running time with K-Means: 6.604s
Running time with Composite (KD-Tree and K-Means): 5.219s
algorithm : 2
branching : 16
centers_init : 0
iterations : 5
checks : 84
eps : 0
sorted : 1
Running time with Autotuned based on data: 120.67s

Comparing to: ../../testdata/Wall/img6.png
Number of SURF Keypoints: 7564
Running time with BruteForce: 7.44s
Running time with BruteForce-L1: 6.947s
Running time with BruteForce-SL2: 7.057s
Running time with FlannBased: 7.193s
Running time with Linear Brute Force: 15.364s
Running time with KD-Tree (4 trees): 6.375s
Running time with K-Means: 4.426s
Running time with Composite (KD-Tree and K-Means): 4.401s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 86.696s

BruteForce time taken: 148.124s
BruteForce-L1 time taken: 139.256s
BruteForce-Hamming time taken: 0s
BruteForce-Hamming(2) time taken: 0s
BruteForce-SL2 time taken: 138.179s
FlannBased time taken: 190.377s
Linear Brute Force time taken: 332.272s
KD-Tree (4 trees) time taken: 185.886s
K-Means time taken: 139.674s
Composite (KD-Tree and K-Means) time taken: 138.025s
Locality Sensitive Hashing time taken: 0s
Autotuned based on data time taken: 2795.07s

Total average number of keypoints: 5237
Total run-time for matching evaluation: 4640.11s