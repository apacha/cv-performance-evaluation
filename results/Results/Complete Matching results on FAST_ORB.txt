Scene: Bark
Number of FAST Keypoints: 500

Comparing to: ../../testdata/Bark/img2.png
Number of FAST Keypoints: 500
Running time with BruteForce: 0.012s
Running time with BruteForce-L1: 0.009s
Running time with BruteForce-Hamming: 0.01s
Running time with BruteForce-Hamming(2): 0.012s
Running time with BruteForce-SL2: 0.012s
Running time with FlannBased: 0.235s
Running time with Linear Brute Force: 0.04s
Running time with KD-Tree (4 trees): 0.232s
Running time with K-Means: 0.068s
Running time with Composite (KD-Tree and K-Means): 0.074s
Running time with Locality Sensitive Hashing: 0.486s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 0.248s

Comparing to: ../../testdata/Bark/img3.png
Number of FAST Keypoints: 500
Running time with BruteForce: 0.013s
Running time with BruteForce-L1: 0.009s
Running time with BruteForce-Hamming: 0.009s
Running time with BruteForce-Hamming(2): 0.012s
Running time with BruteForce-SL2: 0.011s
Running time with FlannBased: 0.239s
Running time with Linear Brute Force: 0.04s
Running time with KD-Tree (4 trees): 0.256s
Running time with K-Means: 0.075s
Running time with Composite (KD-Tree and K-Means): 0.076s
Running time with Locality Sensitive Hashing: 0.478s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 0.25s

Comparing to: ../../testdata/Bark/img4.png
Number of FAST Keypoints: 500
Running time with BruteForce: 0.012s
Running time with BruteForce-L1: 0.009s
Running time with BruteForce-Hamming: 0.009s
Running time with BruteForce-Hamming(2): 0.012s
Running time with BruteForce-SL2: 0.01s
Running time with FlannBased: 0.223s
Running time with Linear Brute Force: 0.038s
Running time with KD-Tree (4 trees): 0.247s
Running time with K-Means: 0.07s
Running time with Composite (KD-Tree and K-Means): 0.095s
Running time with Locality Sensitive Hashing: 0.623s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 0.249s

Comparing to: ../../testdata/Bark/img5.png
Number of FAST Keypoints: 500
Running time with BruteForce: 0.011s
Running time with BruteForce-L1: 0.009s
Running time with BruteForce-Hamming: 0.008s
Running time with BruteForce-Hamming(2): 0.012s
Running time with BruteForce-SL2: 0.009s
Running time with FlannBased: 0.206s
Running time with Linear Brute Force: 0.038s
Running time with KD-Tree (4 trees): 0.205s
Running time with K-Means: 0.071s
Running time with Composite (KD-Tree and K-Means): 0.061s
Running time with Locality Sensitive Hashing: 0.418s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 0.247s

Comparing to: ../../testdata/Bark/img6.png
Number of FAST Keypoints: 500
Running time with BruteForce: 0.013s
Running time with BruteForce-L1: 0.008s
Running time with BruteForce-Hamming: 0.01s
Running time with BruteForce-Hamming(2): 0.013s
Running time with BruteForce-SL2: 0.011s
Running time with FlannBased: 0.26s
Running time with Linear Brute Force: 0.046s
Running time with KD-Tree (4 trees): 0.301s
Running time with K-Means: 0.105s
Running time with Composite (KD-Tree and K-Means): 0.085s
Running time with Locality Sensitive Hashing: 0.521s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 0.252s

Scene: Bikes
Number of FAST Keypoints: 500

Comparing to: ../../testdata/Bikes/img2.png
Number of FAST Keypoints: 500
Running time with BruteForce: 0.013s
Running time with BruteForce-L1: 0.011s
Running time with BruteForce-Hamming: 0.01s
Running time with BruteForce-Hamming(2): 0.016s
Running time with BruteForce-SL2: 0.012s
Running time with FlannBased: 0.246s
Running time with Linear Brute Force: 0.048s
Running time with KD-Tree (4 trees): 0.294s
Running time with K-Means: 0.106s
Running time with Composite (KD-Tree and K-Means): 0.12s
Running time with Locality Sensitive Hashing: 0.556s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 0.259s

Comparing to: ../../testdata/Bikes/img3.png
Number of FAST Keypoints: 500
Running time with BruteForce: 0.013s
Running time with BruteForce-L1: 0.01s
Running time with BruteForce-Hamming: 0.01s
Running time with BruteForce-Hamming(2): 0.016s
Running time with BruteForce-SL2: 0.011s
Running time with FlannBased: 0.275s
Running time with Linear Brute Force: 0.042s
Running time with KD-Tree (4 trees): 0.265s
Running time with K-Means: 0.087s
Running time with Composite (KD-Tree and K-Means): 0.081s
Running time with Locality Sensitive Hashing: 0.644s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 0.254s

Comparing to: ../../testdata/Bikes/img4.png
Number of FAST Keypoints: 500
Running time with BruteForce: 0.014s
Running time with BruteForce-L1: 0.009s
Running time with BruteForce-Hamming: 0.01s
Running time with BruteForce-Hamming(2): 0.013s
Running time with BruteForce-SL2: 0.012s
Running time with FlannBased: 0.266s
Running time with Linear Brute Force: 0.043s
Running time with KD-Tree (4 trees): 0.306s
Running time with K-Means: 0.079s
Running time with Composite (KD-Tree and K-Means): 0.085s
Running time with Locality Sensitive Hashing: 0.558s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 0.278s

Comparing to: ../../testdata/Bikes/img5.png
Number of FAST Keypoints: 500
Running time with BruteForce: 0.019s
Running time with BruteForce-L1: 0.011s
Running time with BruteForce-Hamming: 0.015s
Running time with BruteForce-Hamming(2): 0.019s
Running time with BruteForce-SL2: 0.016s
Running time with FlannBased: 0.295s
Running time with Linear Brute Force: 0.045s
Running time with KD-Tree (4 trees): 0.251s
Running time with K-Means: 0.089s
Running time with Composite (KD-Tree and K-Means): 0.082s
Running time with Locality Sensitive Hashing: 0.649s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 0.256s

Comparing to: ../../testdata/Bikes/img6.png
Number of FAST Keypoints: 331
Running time with BruteForce: 0.009s
Running time with BruteForce-L1: 0.006s
Running time with BruteForce-Hamming: 0.007s
Running time with BruteForce-Hamming(2): 0.009s
Running time with BruteForce-SL2: 0.01s
Running time with FlannBased: 0.167s
Running time with Linear Brute Force: 0.029s
Running time with KD-Tree (4 trees): 0.218s
Running time with K-Means: 0.071s
Running time with Composite (KD-Tree and K-Means): 0.061s
Running time with Locality Sensitive Hashing: 0.407s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 0.241s

Scene: Boat
Number of FAST Keypoints: 500

Comparing to: ../../testdata/Boat/img2.png
Number of FAST Keypoints: 500
Running time with BruteForce: 0.016s
Running time with BruteForce-L1: 0.012s
Running time with BruteForce-Hamming: 0.012s
Running time with BruteForce-Hamming(2): 0.016s
Running time with BruteForce-SL2: 0.015s
Running time with FlannBased: 0.256s
Running time with Linear Brute Force: 0.047s
Running time with KD-Tree (4 trees): 0.293s
Running time with K-Means: 0.091s
Running time with Composite (KD-Tree and K-Means): 0.092s
Running time with Locality Sensitive Hashing: 0.625s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 0.258s

Comparing to: ../../testdata/Boat/img3.png
Number of FAST Keypoints: 500
Running time with BruteForce: 0.016s
Running time with BruteForce-L1: 0.01s
Running time with BruteForce-Hamming: 0.011s
Running time with BruteForce-Hamming(2): 0.016s
Running time with BruteForce-SL2: 0.013s
Running time with FlannBased: 0.274s
Running time with Linear Brute Force: 0.049s
Running time with KD-Tree (4 trees): 0.295s
Running time with K-Means: 0.115s
Running time with Composite (KD-Tree and K-Means): 0.094s
Running time with Locality Sensitive Hashing: 0.566s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 0.261s

Comparing to: ../../testdata/Boat/img4.png
Number of FAST Keypoints: 500
Running time with BruteForce: 0.015s
Running time with BruteForce-L1: 0.01s
Running time with BruteForce-Hamming: 0.012s
Running time with BruteForce-Hamming(2): 0.016s
Running time with BruteForce-SL2: 0.014s
Running time with FlannBased: 0.253s
Running time with Linear Brute Force: 0.046s
Running time with KD-Tree (4 trees): 0.269s
Running time with K-Means: 0.083s
Running time with Composite (KD-Tree and K-Means): 0.105s
Running time with Locality Sensitive Hashing: 0.603s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 0.28s

Comparing to: ../../testdata/Boat/img5.png
Number of FAST Keypoints: 500
Running time with BruteForce: 0.014s
Running time with BruteForce-L1: 0.01s
Running time with BruteForce-Hamming: 0.012s
Running time with BruteForce-Hamming(2): 0.016s
Running time with BruteForce-SL2: 0.014s
Running time with FlannBased: 0.311s
Running time with Linear Brute Force: 0.054s
Running time with KD-Tree (4 trees): 0.261s
Running time with K-Means: 0.078s
Running time with Composite (KD-Tree and K-Means): 0.086s
Running time with Locality Sensitive Hashing: 0.599s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 0.264s

Comparing to: ../../testdata/Boat/img6.png
Number of FAST Keypoints: 500
Running time with BruteForce: 0.016s
Running time with BruteForce-L1: 0.009s
Running time with BruteForce-Hamming: 0.011s
Running time with BruteForce-Hamming(2): 0.015s
Running time with BruteForce-SL2: 0.018s
Running time with FlannBased: 0.278s
Running time with Linear Brute Force: 0.05s
Running time with KD-Tree (4 trees): 0.286s
Running time with K-Means: 0.089s
Running time with Composite (KD-Tree and K-Means): 0.079s
Running time with Locality Sensitive Hashing: 0.594s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 0.261s

Scene: Christchurch
Number of FAST Keypoints: 500

Comparing to: ../../testdata/Christchurch/img2.png
Number of FAST Keypoints: 500
Running time with BruteForce: 0.016s
Running time with BruteForce-L1: 0.011s
Running time with BruteForce-Hamming: 0.012s
Running time with BruteForce-Hamming(2): 0.022s
Running time with BruteForce-SL2: 0.019s
Running time with FlannBased: 0.284s
Running time with Linear Brute Force: 0.047s
Running time with KD-Tree (4 trees): 0.283s
Running time with K-Means: 0.087s
Running time with Composite (KD-Tree and K-Means): 0.096s
Running time with Locality Sensitive Hashing: 0.649s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 0.26s

Comparing to: ../../testdata/Christchurch/img3.png
Number of FAST Keypoints: 500
Running time with BruteForce: 0.015s
Running time with BruteForce-L1: 0.011s
Running time with BruteForce-Hamming: 0.012s
Running time with BruteForce-Hamming(2): 0.018s
Running time with BruteForce-SL2: 0.014s
Running time with FlannBased: 0.278s
Running time with Linear Brute Force: 0.043s
Running time with KD-Tree (4 trees): 0.252s
Running time with K-Means: 0.079s
Running time with Composite (KD-Tree and K-Means): 0.113s
Running time with Locality Sensitive Hashing: 0.586s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 0.256s

Comparing to: ../../testdata/Christchurch/img4.png
Number of FAST Keypoints: 500
Running time with BruteForce: 0.016s
Running time with BruteForce-L1: 0.011s
Running time with BruteForce-Hamming: 0.011s
Running time with BruteForce-Hamming(2): 0.015s
Running time with BruteForce-SL2: 0.013s
Running time with FlannBased: 0.31s
Running time with Linear Brute Force: 0.044s
Running time with KD-Tree (4 trees): 0.27s
Running time with K-Means: 0.127s
Running time with Composite (KD-Tree and K-Means): 0.095s
Running time with Locality Sensitive Hashing: 0.693s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 0.264s

Comparing to: ../../testdata/Christchurch/img5.png
Number of FAST Keypoints: 500
Running time with BruteForce: 0.014s
Running time with BruteForce-L1: 0.01s
Running time with BruteForce-Hamming: 0.011s
Running time with BruteForce-Hamming(2): 0.016s
Running time with BruteForce-SL2: 0.013s
Running time with FlannBased: 0.262s
Running time with Linear Brute Force: 0.044s
Running time with KD-Tree (4 trees): 0.251s
Running time with K-Means: 0.096s
Running time with Composite (KD-Tree and K-Means): 0.09s
Running time with Locality Sensitive Hashing: 0.617s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 0.259s

Comparing to: ../../testdata/Christchurch/img6.png
Number of FAST Keypoints: 500
Running time with BruteForce: 0.014s
Running time with BruteForce-L1: 0.01s
Running time with BruteForce-Hamming: 0.011s
Running time with BruteForce-Hamming(2): 0.014s
Running time with BruteForce-SL2: 0.012s
Running time with FlannBased: 0.255s
Running time with Linear Brute Force: 0.044s
Running time with KD-Tree (4 trees): 0.288s
Running time with K-Means: 0.085s
Running time with Composite (KD-Tree and K-Means): 0.076s
Running time with Locality Sensitive Hashing: 0.605s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 0.256s

Scene: Graf
Number of FAST Keypoints: 500

Comparing to: ../../testdata/Graf/img2.png
Number of FAST Keypoints: 500
Running time with BruteForce: 0.012s
Running time with BruteForce-L1: 0.009s
Running time with BruteForce-Hamming: 0.009s
Running time with BruteForce-Hamming(2): 0.013s
Running time with BruteForce-SL2: 0.01s
Running time with FlannBased: 0.245s
Running time with Linear Brute Force: 0.039s
Running time with KD-Tree (4 trees): 0.234s
Running time with K-Means: 0.07s
Running time with Composite (KD-Tree and K-Means): 0.067s
Running time with Locality Sensitive Hashing: 0.46s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 0.266s

Comparing to: ../../testdata/Graf/img3.png
Number of FAST Keypoints: 500
Running time with BruteForce: 0.016s
Running time with BruteForce-L1: 0.007s
Running time with BruteForce-Hamming: 0.008s
Running time with BruteForce-Hamming(2): 0.012s
Running time with BruteForce-SL2: 0.011s
Running time with FlannBased: 0.276s
Running time with Linear Brute Force: 0.05s
Running time with KD-Tree (4 trees): 0.23s
Running time with K-Means: 0.068s
Running time with Composite (KD-Tree and K-Means): 0.079s
Running time with Locality Sensitive Hashing: 0.532s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 0.248s

Comparing to: ../../testdata/Graf/img4.png
Number of FAST Keypoints: 500
Running time with BruteForce: 0.012s
Running time with BruteForce-L1: 0.009s
Running time with BruteForce-Hamming: 0.009s
Running time with BruteForce-Hamming(2): 0.016s
Running time with BruteForce-SL2: 0.014s
Running time with FlannBased: 0.318s
Running time with Linear Brute Force: 0.038s
Running time with KD-Tree (4 trees): 0.225s
Running time with K-Means: 0.078s
Running time with Composite (KD-Tree and K-Means): 0.074s
Running time with Locality Sensitive Hashing: 0.516s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 0.248s

Comparing to: ../../testdata/Graf/img5.png
Number of FAST Keypoints: 500
Running time with BruteForce: 0.02s
Running time with BruteForce-L1: 0.011s
Running time with BruteForce-Hamming: 0.012s
Running time with BruteForce-Hamming(2): 0.013s
Running time with BruteForce-SL2: 0.011s
Running time with FlannBased: 0.237s
Running time with Linear Brute Force: 0.045s
Running time with KD-Tree (4 trees): 0.283s
Running time with K-Means: 0.07s
Running time with Composite (KD-Tree and K-Means): 0.072s
Running time with Locality Sensitive Hashing: 0.485s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 0.25s

Comparing to: ../../testdata/Graf/img6.png
Number of FAST Keypoints: 500
Running time with BruteForce: 0.011s
Running time with BruteForce-L1: 0.008s
Running time with BruteForce-Hamming: 0.007s
Running time with BruteForce-Hamming(2): 0.016s
Running time with BruteForce-SL2: 0.009s
Running time with FlannBased: 0.208s
Running time with Linear Brute Force: 0.034s
Running time with KD-Tree (4 trees): 0.202s
Running time with K-Means: 0.07s
Running time with Composite (KD-Tree and K-Means): 0.066s
Running time with Locality Sensitive Hashing: 0.426s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 0.245s

Scene: Leuven
Number of FAST Keypoints: 500

Comparing to: ../../testdata/Leuven/img2.png
Number of FAST Keypoints: 500
Running time with BruteForce: 0.009s
Running time with BruteForce-L1: 0.007s
Running time with BruteForce-Hamming: 0.008s
Running time with BruteForce-Hamming(2): 0.01s
Running time with BruteForce-SL2: 0.01s
Running time with FlannBased: 0.193s
Running time with Linear Brute Force: 0.034s
Running time with KD-Tree (4 trees): 0.183s
Running time with K-Means: 0.062s
Running time with Composite (KD-Tree and K-Means): 0.058s
Running time with Locality Sensitive Hashing: 0.378s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 0.242s

Comparing to: ../../testdata/Leuven/img3.png
Number of FAST Keypoints: 500
Running time with BruteForce: 0.009s
Running time with BruteForce-L1: 0.006s
Running time with BruteForce-Hamming: 0.007s
Running time with BruteForce-Hamming(2): 0.009s
Running time with BruteForce-SL2: 0.008s
Running time with FlannBased: 0.188s
Running time with Linear Brute Force: 0.03s
Running time with KD-Tree (4 trees): 0.185s
Running time with K-Means: 0.062s
Running time with Composite (KD-Tree and K-Means): 0.057s
Running time with Locality Sensitive Hashing: 0.469s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 0.242s

Comparing to: ../../testdata/Leuven/img4.png
Number of FAST Keypoints: 500
Running time with BruteForce: 0.009s
Running time with BruteForce-L1: 0.007s
Running time with BruteForce-Hamming: 0.007s
Running time with BruteForce-Hamming(2): 0.01s
Running time with BruteForce-SL2: 0.008s
Running time with FlannBased: 0.188s
Running time with Linear Brute Force: 0.032s
Running time with KD-Tree (4 trees): 0.189s
Running time with K-Means: 0.059s
Running time with Composite (KD-Tree and K-Means): 0.056s
Running time with Locality Sensitive Hashing: 0.454s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 0.245s

Comparing to: ../../testdata/Leuven/img5.png
Number of FAST Keypoints: 500
Running time with BruteForce: 0.01s
Running time with BruteForce-L1: 0.007s
Running time with BruteForce-Hamming: 0.008s
Running time with BruteForce-Hamming(2): 0.011s
Running time with BruteForce-SL2: 0.01s
Running time with FlannBased: 0.211s
Running time with Linear Brute Force: 0.032s
Running time with KD-Tree (4 trees): 0.197s
Running time with K-Means: 0.06s
Running time with Composite (KD-Tree and K-Means): 0.063s
Running time with Locality Sensitive Hashing: 0.431s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 0.251s

Comparing to: ../../testdata/Leuven/img6.png
Number of FAST Keypoints: 500
Running time with BruteForce: 0.01s
Running time with BruteForce-L1: 0.008s
Running time with BruteForce-Hamming: 0.007s
Running time with BruteForce-Hamming(2): 0.01s
Running time with BruteForce-SL2: 0.008s
Running time with FlannBased: 0.2s
Running time with Linear Brute Force: 0.032s
Running time with KD-Tree (4 trees): 0.185s
Running time with K-Means: 0.058s
Running time with Composite (KD-Tree and K-Means): 0.069s
Running time with Locality Sensitive Hashing: 0.405s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 0.248s

Scene: Memorial
Number of FAST Keypoints: 500

Comparing to: ../../testdata/Memorial/img2.png
Number of FAST Keypoints: 500
Running time with BruteForce: 0.014s
Running time with BruteForce-L1: 0.01s
Running time with BruteForce-Hamming: 0.011s
Running time with BruteForce-Hamming(2): 0.014s
Running time with BruteForce-SL2: 0.012s
Running time with FlannBased: 0.242s
Running time with Linear Brute Force: 0.044s
Running time with KD-Tree (4 trees): 0.237s
Running time with K-Means: 0.078s
Running time with Composite (KD-Tree and K-Means): 0.085s
Running time with Locality Sensitive Hashing: 0.582s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 0.255s

Comparing to: ../../testdata/Memorial/img3.png
Number of FAST Keypoints: 500
Running time with BruteForce: 0.015s
Running time with BruteForce-L1: 0.009s
Running time with BruteForce-Hamming: 0.011s
Running time with BruteForce-Hamming(2): 0.015s
Running time with BruteForce-SL2: 0.013s
Running time with FlannBased: 0.262s
Running time with Linear Brute Force: 0.047s
Running time with KD-Tree (4 trees): 0.262s
Running time with K-Means: 0.081s
Running time with Composite (KD-Tree and K-Means): 0.086s
Running time with Locality Sensitive Hashing: 0.692s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 0.259s

Comparing to: ../../testdata/Memorial/img4.png
Number of FAST Keypoints: 500
Running time with BruteForce: 0.013s
Running time with BruteForce-L1: 0.009s
Running time with BruteForce-Hamming: 0.01s
Running time with BruteForce-Hamming(2): 0.013s
Running time with BruteForce-SL2: 0.012s
Running time with FlannBased: 0.242s
Running time with Linear Brute Force: 0.043s
Running time with KD-Tree (4 trees): 0.236s
Running time with K-Means: 0.085s
Running time with Composite (KD-Tree and K-Means): 0.078s
Running time with Locality Sensitive Hashing: 0.518s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 0.253s

Comparing to: ../../testdata/Memorial/img5.png
Number of FAST Keypoints: 500
Running time with BruteForce: 0.014s
Running time with BruteForce-L1: 0.009s
Running time with BruteForce-Hamming: 0.01s
Running time with BruteForce-Hamming(2): 0.014s
Running time with BruteForce-SL2: 0.012s
Running time with FlannBased: 0.241s
Running time with Linear Brute Force: 0.042s
Running time with KD-Tree (4 trees): 0.227s
Running time with K-Means: 0.076s
Running time with Composite (KD-Tree and K-Means): 0.078s
Running time with Locality Sensitive Hashing: 0.504s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 0.253s

Comparing to: ../../testdata/Memorial/img6.png
Number of FAST Keypoints: 500
Running time with BruteForce: 0.015s
Running time with BruteForce-L1: 0.011s
Running time with BruteForce-Hamming: 0.01s
Running time with BruteForce-Hamming(2): 0.015s
Running time with BruteForce-SL2: 0.013s
Running time with FlannBased: 0.248s
Running time with Linear Brute Force: 0.044s
Running time with KD-Tree (4 trees): 0.24s
Running time with K-Means: 0.088s
Running time with Composite (KD-Tree and K-Means): 0.079s
Running time with Locality Sensitive Hashing: 0.591s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 0.272s

Scene: Trees
Number of FAST Keypoints: 500

Comparing to: ../../testdata/Trees/img2.png
Number of FAST Keypoints: 500
Running time with BruteForce: 0.012s
Running time with BruteForce-L1: 0.009s
Running time with BruteForce-Hamming: 0.009s
Running time with BruteForce-Hamming(2): 0.013s
Running time with BruteForce-SL2: 0.011s
Running time with FlannBased: 0.234s
Running time with Linear Brute Force: 0.042s
Running time with KD-Tree (4 trees): 0.239s
Running time with K-Means: 0.076s
Running time with Composite (KD-Tree and K-Means): 0.075s
Running time with Locality Sensitive Hashing: 0.501s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 0.253s

Comparing to: ../../testdata/Trees/img3.png
Number of FAST Keypoints: 500
Running time with BruteForce: 0.015s
Running time with BruteForce-L1: 0.01s
Running time with BruteForce-Hamming: 0.009s
Running time with BruteForce-Hamming(2): 0.014s
Running time with BruteForce-SL2: 0.012s
Running time with FlannBased: 0.242s
Running time with Linear Brute Force: 0.042s
Running time with KD-Tree (4 trees): 0.241s
Running time with K-Means: 0.077s
Running time with Composite (KD-Tree and K-Means): 0.082s
Running time with Locality Sensitive Hashing: 0.52s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 0.254s

Comparing to: ../../testdata/Trees/img4.png
Number of FAST Keypoints: 500
Running time with BruteForce: 0.014s
Running time with BruteForce-L1: 0.009s
Running time with BruteForce-Hamming: 0.01s
Running time with BruteForce-Hamming(2): 0.014s
Running time with BruteForce-SL2: 0.012s
Running time with FlannBased: 0.253s
Running time with Linear Brute Force: 0.043s
Running time with KD-Tree (4 trees): 0.251s
Running time with K-Means: 0.073s
Running time with Composite (KD-Tree and K-Means): 0.073s
Running time with Locality Sensitive Hashing: 0.537s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 0.255s

Comparing to: ../../testdata/Trees/img5.png
Number of FAST Keypoints: 500
Running time with BruteForce: 0.013s
Running time with BruteForce-L1: 0.01s
Running time with BruteForce-Hamming: 0.009s
Running time with BruteForce-Hamming(2): 0.013s
Running time with BruteForce-SL2: 0.011s
Running time with FlannBased: 0.232s
Running time with Linear Brute Force: 0.04s
Running time with KD-Tree (4 trees): 0.23s
Running time with K-Means: 0.075s
Running time with Composite (KD-Tree and K-Means): 0.068s
Running time with Locality Sensitive Hashing: 0.504s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 0.253s

Comparing to: ../../testdata/Trees/img6.png
Number of FAST Keypoints: 500
Running time with BruteForce: 0.012s
Running time with BruteForce-L1: 0.009s
Running time with BruteForce-Hamming: 0.009s
Running time with BruteForce-Hamming(2): 0.013s
Running time with BruteForce-SL2: 0.011s
Running time with FlannBased: 0.224s
Running time with Linear Brute Force: 0.039s
Running time with KD-Tree (4 trees): 0.226s
Running time with K-Means: 0.072s
Running time with Composite (KD-Tree and K-Means): 0.085s
Running time with Locality Sensitive Hashing: 0.492s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 0.251s

Scene: Ubc
Number of FAST Keypoints: 500

Comparing to: ../../testdata/Ubc/img2.png
Number of FAST Keypoints: 500
Running time with BruteForce: 0.013s
Running time with BruteForce-L1: 0.009s
Running time with BruteForce-Hamming: 0.01s
Running time with BruteForce-Hamming(2): 0.013s
Running time with BruteForce-SL2: 0.011s
Running time with FlannBased: 0.177s
Running time with Linear Brute Force: 0.043s
Running time with KD-Tree (4 trees): 0.174s
Running time with K-Means: 0.073s
Running time with Composite (KD-Tree and K-Means): 0.078s
Running time with Locality Sensitive Hashing: 0.486s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 0.254s

Comparing to: ../../testdata/Ubc/img3.png
Number of FAST Keypoints: 500
Running time with BruteForce: 0.013s
Running time with BruteForce-L1: 0.009s
Running time with BruteForce-Hamming: 0.01s
Running time with BruteForce-Hamming(2): 0.013s
Running time with BruteForce-SL2: 0.012s
Running time with FlannBased: 0.203s
Running time with Linear Brute Force: 0.043s
Running time with KD-Tree (4 trees): 0.204s
Running time with K-Means: 0.082s
Running time with Composite (KD-Tree and K-Means): 0.071s
Running time with Locality Sensitive Hashing: 0.48s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 0.255s

Comparing to: ../../testdata/Ubc/img4.png
Number of FAST Keypoints: 500
Running time with BruteForce: 0.013s
Running time with BruteForce-L1: 0.009s
Running time with BruteForce-Hamming: 0.011s
Running time with BruteForce-Hamming(2): 0.012s
Running time with BruteForce-SL2: 0.011s
Running time with FlannBased: 0.224s
Running time with Linear Brute Force: 0.041s
Running time with KD-Tree (4 trees): 0.222s
Running time with K-Means: 0.069s
Running time with Composite (KD-Tree and K-Means): 0.079s
Running time with Locality Sensitive Hashing: 0.485s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 0.254s

Comparing to: ../../testdata/Ubc/img5.png
Number of FAST Keypoints: 500
Running time with BruteForce: 0.013s
Running time with BruteForce-L1: 0.008s
Running time with BruteForce-Hamming: 0.01s
Running time with BruteForce-Hamming(2): 0.013s
Running time with BruteForce-SL2: 0.011s
Running time with FlannBased: 0.23s
Running time with Linear Brute Force: 0.041s
Running time with KD-Tree (4 trees): 0.231s
Running time with K-Means: 0.066s
Running time with Composite (KD-Tree and K-Means): 0.067s
Running time with Locality Sensitive Hashing: 0.471s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 0.254s

Comparing to: ../../testdata/Ubc/img6.png
Number of FAST Keypoints: 500
Running time with BruteForce: 0.013s
Running time with BruteForce-L1: 0.009s
Running time with BruteForce-Hamming: 0.01s
Running time with BruteForce-Hamming(2): 0.013s
Running time with BruteForce-SL2: 0.011s
Running time with FlannBased: 0.236s
Running time with Linear Brute Force: 0.042s
Running time with KD-Tree (4 trees): 0.231s
Running time with K-Means: 0.07s
Running time with Composite (KD-Tree and K-Means): 0.073s
Running time with Locality Sensitive Hashing: 0.513s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 0.255s

Scene: Wall
Number of FAST Keypoints: 500

Comparing to: ../../testdata/Wall/img2.png
Number of FAST Keypoints: 500
Running time with BruteForce: 0.017s
Running time with BruteForce-L1: 0.009s
Running time with BruteForce-Hamming: 0.013s
Running time with BruteForce-Hamming(2): 0.017s
Running time with BruteForce-SL2: 0.014s
Running time with FlannBased: 0.253s
Running time with Linear Brute Force: 0.042s
Running time with KD-Tree (4 trees): 0.238s
Running time with K-Means: 0.072s
Running time with Composite (KD-Tree and K-Means): 0.08s
Running time with Locality Sensitive Hashing: 0.532s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 0.254s

Comparing to: ../../testdata/Wall/img3.png
Number of FAST Keypoints: 500
Running time with BruteForce: 0.013s
Running time with BruteForce-L1: 0.009s
Running time with BruteForce-Hamming: 0.008s
Running time with BruteForce-Hamming(2): 0.013s
Running time with BruteForce-SL2: 0.01s
Running time with FlannBased: 0.23s
Running time with Linear Brute Force: 0.039s
Running time with KD-Tree (4 trees): 0.224s
Running time with K-Means: 0.076s
Running time with Composite (KD-Tree and K-Means): 0.082s
Running time with Locality Sensitive Hashing: 0.48s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 0.25s

Comparing to: ../../testdata/Wall/img4.png
Number of FAST Keypoints: 500
Running time with BruteForce: 0.013s
Running time with BruteForce-L1: 0.01s
Running time with BruteForce-Hamming: 0.01s
Running time with BruteForce-Hamming(2): 0.013s
Running time with BruteForce-SL2: 0.011s
Running time with FlannBased: 0.261s
Running time with Linear Brute Force: 0.041s
Running time with KD-Tree (4 trees): 0.235s
Running time with K-Means: 0.069s
Running time with Composite (KD-Tree and K-Means): 0.081s
Running time with Locality Sensitive Hashing: 0.486s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 0.25s

Comparing to: ../../testdata/Wall/img5.png
Number of FAST Keypoints: 500
Running time with BruteForce: 0.014s
Running time with BruteForce-L1: 0.008s
Running time with BruteForce-Hamming: 0.009s
Running time with BruteForce-Hamming(2): 0.013s
Running time with BruteForce-SL2: 0.011s
Running time with FlannBased: 0.224s
Running time with Linear Brute Force: 0.039s
Running time with KD-Tree (4 trees): 0.23s
Running time with K-Means: 0.066s
Running time with Composite (KD-Tree and K-Means): 0.074s
Running time with Locality Sensitive Hashing: 0.483s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 0.251s

Comparing to: ../../testdata/Wall/img6.png
Number of FAST Keypoints: 500
Running time with BruteForce: 0.013s
Running time with BruteForce-L1: 0.009s
Running time with BruteForce-Hamming: 0.01s
Running time with BruteForce-Hamming(2): 0.013s
Running time with BruteForce-SL2: 0.011s
Running time with FlannBased: 0.239s
Running time with Linear Brute Force: 0.04s
Running time with KD-Tree (4 trees): 0.233s
Running time with K-Means: 0.076s
Running time with Composite (KD-Tree and K-Means): 0.07s
Running time with Locality Sensitive Hashing: 0.496s
algorithm : 0
checks : 1
eps : 0
sorted : 1
Running time with Autotuned based on data: 0.252s

BruteForce time taken: 0.671s
BruteForce-L1 time taken: 0.458s
BruteForce-Hamming time taken: 0.494s
BruteForce-Hamming(2) time taken: 0.689s
BruteForce-SL2 time taken: 0.59s
FlannBased time taken: 12.134s
Linear Brute Force time taken: 2.08s
KD-Tree (4 trees) time taken: 12.047s
K-Means time taken: 3.908s
Composite (KD-Tree and K-Means) time taken: 3.951s
Locality Sensitive Hashing time taken: 26.386s
Autotuned based on data time taken: 12.725s
Total average number of keypoints: 497
Total run-time for matching evaluation: 86.294s
