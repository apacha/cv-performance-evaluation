/* Code by Tom Botterill. Documentation and license at http://www.hilandtom.com/tombotterill/code */

/*
 * openCV8PtEssentialMatModified.cpp
 *
 *  Created on: 8/10/2009
 *      Author: tom
 */

#include "openCV4PtHomography.h"
#include "util/opencv.h"
#include "ransac.h"

#ifndef USE_OLD_OPENCV
#include "opencv2/calib3d/calib3d.hpp"
#endif
#include "opencv/cv.h"//cvFindHomography has been moved here in the latest OpenCV

using namespace std;

int COpenCV4ptHomography::getModels_int(const TSubSet & anHypSet, CModels & pModels)
{
	const C3x3MatModel & model = dynamic_cast<const C3x3MatModel &>(pModels.addModel());

	CvMat H = cvMat(3, 3, CV_64FC1, const_cast<double *>(model.asDouble9()));

	double adPoints1_data[2*4];
	double adPoints2_data[2*4];

	CvMat CvMat1 = cvMat(nPoints, 2, CV_64FC1, adPoints1_data);
	CvMat CvMat2 = cvMat(nPoints, 2, CV_64FC1, adPoints2_data);

	for(int i=0; i<4; i++)
	{
		cvmSet(&CvMat1, i, 0, p1[anHypSet[i]].getX());
		cvmSet(&CvMat1, i, 1, p1[anHypSet[i]].getY());
		cvmSet(&CvMat2, i, 0, p2[anHypSet[i]].getX());
		cvmSet(&CvMat2, i, 1, p2[anHypSet[i]].getY());
	}

	cvFindHomography( &CvMat1, &CvMat2, &H);

	return pModels.numModels();
}

bool COpenCVHomography::fitModel_int(CMask & mask, CModel & pModel, bool bVerbose)
{
	const C3x3MatModel & model = dynamic_cast<const C3x3MatModel &>(pModel);
    int nInliers = mask.countInliers();
	if(nInliers < 4) return false;

    const int nPoints = p1.size();

	CvMat H = cvMat(3, 3, CV_64FC1, const_cast<double *>(model.asDouble9()));

	CvMat * pCvMat1 = cvCreateMat(nInliers, 2, CV_64FC1);
	CvMat * pCvMat2 = cvCreateMat(nInliers, 2, CV_64FC1);

	/* cvFindHomography DOES NOT SUPPORT MASK
	CvMat * pCvMask = cvCreateMat(nPoints, 1, CV_8UC1);
    for(int i=0; i<nPoints; i++)
	{
		cvmSet(pCvMat1, i, 0, p1[i].getX());
		cvmSet(pCvMat1, i, 1, p1[i].getY());
		cvmSet(pCvMat2, i, 0, p2[i].getX());
		cvmSet(pCvMat2, i, 1, p2[i].getY());
        //Yuk but works
        pCvMask->data.ptr[i] = mask[i];
		//cvmSet(pCvMask, i, 0, mask[i]);
	}

	cvFindHomography( pCvMat1, pCvMat2,
	                       &H, / *int method=* /0,
	                       / * ransacReprojThreshold=* /0, pCvMask);*/

    int nInlier = 0;
    for(int i=0; i<nPoints; i++)
	{
        if(mask[i])
        {
		    cvmSet(pCvMat1, nInlier, 0, p1[i].getX());
		    cvmSet(pCvMat1, nInlier, 1, p1[i].getY());
		    cvmSet(pCvMat2, nInlier, 0, p2[i].getX());
		    cvmSet(pCvMat2, nInlier, 1, p2[i].getY());
            nInlier++;
        }
	}
    CHECK(nInlier != nInliers, "Counting error");

	cvFindHomography( pCvMat1, pCvMat2,
	                       &H, /*int method=*/ 0,
	                       /* ransacReprojThreshold=*/ 0, 0);

    cvReleaseMat(&pCvMat1);
	cvReleaseMat(&pCvMat2);

	//cvReleaseMat(&pCvMask);

	return true;
}
