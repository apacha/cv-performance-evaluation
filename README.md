# CV-Performance Evaluation
This repository contains a performance-evaluation of local feature descriptors, detectors and matching-algorithms for wide-baseline matching.

Different algorithms were tested on a set of 10 different scenes, where each contains 6 different images. The results for the matching and the performance of the algorithms is measured against a ground-truth homography that was computed offline.

Separated from each other, the following local feature detectors will be evaluated according to their run-time and to their quality (repeatability):
* FAST
* STAR
* SIFT
* SURF
* ORB
* BRISK
* MSER
* GFTT
* HARRIS
* Dense
* SimpleBlob

A second evaluation built on 500 ORB or 500 SUFT features runs the following feature descriptors and evaluates their run-time and their performance:
* SIFT
* SURF
* ORB
* BRISK
* BRIEF
* FREAK

A third evaluation built on detected descriptors evaluates the performance of a variety of matching algorithms. This part is not very informative, since only a relative small number of features is detected and therefore Brute-Force matching yields exceptional results:
* Basic Matching algorithms: "BruteForce", "BruteForce-L1", "BruteForce-Hamming", "BruteForce-Hamming(2)", "BruteForce-SL2", "FlannBased"
* FLANN-Based matching algorithms: "Linear Brute Force", "KD-Tree (4 trees)", "K-Means", "Composite (KD-Tree and K-Means)", "Locality Sensitive Hashing", "Autotuned based on data"

A fourth evaluation tries to find the best combinations of Detector/Descriptor/Matcher for this use-case in terms of repeatability, precision and run-time.

All results can be found /results/Results.xlsx.

## Developers
* Alexander Pacha (University of Augsburg, Germany)
* James Head-Mears (University of Canterbury, New Zealand)

## Dependend Packages 
* OpenCV

## Build and Install
* This project can be built in your favourite IDE, using CMake

## License
Released under the MIT license.

Copyright, 2013, by [Alexander Pacha](http://my-it.at), James Head-Mears.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


RoStAR, the original name of this project stands for Robust Stable AR as this project was originally intended to serve a different purpose.
