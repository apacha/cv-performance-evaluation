% Convert all ppm-files into png-images
cd('Bark');
imwrite(imread('img1.ppm'),'img1.png','png');
imwrite(imread('img2.ppm'),'img2.png','png');
imwrite(imread('img3.ppm'),'img3.png','png');
imwrite(imread('img4.ppm'),'img4.png','png');
imwrite(imread('img5.ppm'),'img5.png','png');
imwrite(imread('img6.ppm'),'img6.png','png');

cd('../Bikes');
imwrite(imread('img1.ppm'),'img1.png','png');
imwrite(imread('img2.ppm'),'img2.png','png');
imwrite(imread('img3.ppm'),'img3.png','png');
imwrite(imread('img4.ppm'),'img4.png','png');
imwrite(imread('img5.ppm'),'img5.png','png');
imwrite(imread('img6.ppm'),'img6.png','png');

cd('../Boat');
imwrite(imread('img1.pgm'),'img1.png','png');
imwrite(imread('img2.pgm'),'img2.png','png');
imwrite(imread('img3.pgm'),'img3.png','png');
imwrite(imread('img4.pgm'),'img4.png','png');
imwrite(imread('img5.pgm'),'img5.png','png');
imwrite(imread('img6.pgm'),'img6.png','png');

cd('../Graf');
imwrite(imread('img1.ppm'),'img1.png','png');
imwrite(imread('img2.ppm'),'img2.png','png');
imwrite(imread('img3.ppm'),'img3.png','png');
imwrite(imread('img4.ppm'),'img4.png','png');
imwrite(imread('img5.ppm'),'img5.png','png');
imwrite(imread('img6.ppm'),'img6.png','png');

cd('../Leuven');
imwrite(imread('img1.ppm'),'img1.png','png');
imwrite(imread('img2.ppm'),'img2.png','png');
imwrite(imread('img3.ppm'),'img3.png','png');
imwrite(imread('img4.ppm'),'img4.png','png');
imwrite(imread('img5.ppm'),'img5.png','png');
imwrite(imread('img6.ppm'),'img6.png','png');

cd('../Trees');
imwrite(imread('img1.ppm'),'img1.png','png');
imwrite(imread('img2.ppm'),'img2.png','png');
imwrite(imread('img3.ppm'),'img3.png','png');
imwrite(imread('img4.ppm'),'img4.png','png');
imwrite(imread('img5.ppm'),'img5.png','png');
imwrite(imread('img6.ppm'),'img6.png','png');

cd('../Ubc');
imwrite(imread('img1.ppm'),'img1.png','png');
imwrite(imread('img2.ppm'),'img2.png','png');
imwrite(imread('img3.ppm'),'img3.png','png');
imwrite(imread('img4.ppm'),'img4.png','png');
imwrite(imread('img5.ppm'),'img5.png','png');
imwrite(imread('img6.ppm'),'img6.png','png');

cd('../Wall');
imwrite(imread('img1.ppm'),'img1.png','png');
imwrite(imread('img2.ppm'),'img2.png','png');
imwrite(imread('img3.ppm'),'img3.png','png');
imwrite(imread('img4.ppm'),'img4.png','png');
imwrite(imread('img5.ppm'),'img5.png','png');
imwrite(imread('img6.ppm'),'img6.png','png');

